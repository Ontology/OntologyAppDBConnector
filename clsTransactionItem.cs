﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;

namespace OntologyAppDBConnector
{
    public class clsTransactionItem
    {
        private clsOntologyItem _objOItemOntologyItem;
        private clsObjectAtt _objOItemObjectAtt;
        private clsObjectRel _objOItemObjectRel;
        private clsClassAtt _objOItemClassAtt; 
        private clsClassRel _objOItemClassRel;
        private readonly clsClassTypes _objClassTypes = new clsClassTypes();
        public clsOntologyItem TransactionResult { get; set; }
        public bool Removed { get; set; }
        public string SavedType { get; private set; }
        public bool All { get; set; }

        public clsOntologyItem OItemOntologyItem
        {
            get { return _objOItemOntologyItem; }
            set 
            {
                _objOItemOntologyItem = value;
                SavedType = _objClassTypes.ClassType_OntologyItem;
            }
        }

        public clsObjectAtt OItemObjectAtt
        {
            get { return _objOItemObjectAtt; }
            set 
            {
                _objOItemObjectAtt = value;
                SavedType = _objClassTypes.ClassType_ObjectAtt;
            }

        }

        public clsObjectRel OItemObjectRel
        {
            get { return _objOItemObjectRel; }
            set 
            {
                _objOItemObjectRel = value;
                SavedType = _objClassTypes.ClassType_ObjectRel;
            }
        }
    
        public clsClassAtt OItemClassAtt
        {
            get { return _objOItemClassAtt; }
            set 
            {
                _objOItemClassAtt = value;
                SavedType = _objClassTypes.ClassType_ClassAtt;
            }
        }
    
        public clsClassRel OItemClassRel
        {
            get { return _objOItemClassRel; }
            set
            {
                _objOItemClassRel = value;
                SavedType = _objClassTypes.ClassType_ClassRel;
            }
        }
    
        
        public clsTransactionItem()
        {
            All = false;
        }
    }
}
