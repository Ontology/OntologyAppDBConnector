﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.Attributes
{
    public class OntologyClassAttribute : Attribute
    {
        public string IdClass { get; set; }
    }
}
