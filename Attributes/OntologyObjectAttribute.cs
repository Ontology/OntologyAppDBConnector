﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.Attributes
{
    
    public enum PropertyType
    {
        None = 0,
        Guid = 1,
        Name = 2,
        GuidParent = 4
    }
    public class OntologyObjectAttribute : Attribute
    {
        public PropertyType PropertyType { get; set; }
    }
}
