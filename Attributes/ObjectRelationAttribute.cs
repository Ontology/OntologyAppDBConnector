﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.Attributes
{
    public enum RelationSearchType
    {
        Name = 0,
        RegexName = 1,
        ItemProperty = 2,
        OntologyItem = 4
    }
    public class ObjectRelationAttribute : Attribute
    {
        public string IdRelationType { get; set; }
        public string IdParentOther { get; set; }
        public RelationSearchType RelationSearchType { get; set; }
        public bool Replace { get; set; }
        public string ItemPropertyName { get; set; }
    }
}
