﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.Attributes
{
    public class ObjectAttributeAttribute : Attribute
    {
        public string IdAttributeType { get; set; }
        public bool Replace { get; set; }
        public int OrderId { get; set; }

        public ObjectAttributeAttribute()
        {
            OrderId = 1;
        }
    }
}
