﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector
{
    public class clsRelationConfig
    {
        private Globals globals;
        private clsDataTypes dataTypes = new clsDataTypes();
        private OntologyModDBConnector dbConnector_OrderId;

        public clsObjectAtt Rel_ObjectAttribute(clsOntologyItem oItemObject,
            clsOntologyItem oItemAttributeType,
            object value,
            bool doStandardValue = false,
            bool getNextOrderId = false,
            long orderId = 1,
            string idAttribute = null)
        {

            var objAttItem = new clsObjectAtt
            {
                ID_Attribute = idAttribute,
                ID_AttributeType = oItemAttributeType.GUID,
                ID_Object = oItemObject.GUID,
                ID_Class = oItemObject.GUID_Parent,
                ID_DataType = oItemAttributeType.GUID_Parent
            };

            if (oItemAttributeType.GUID_Parent == dataTypes.DType_Bool.GUID)
            {
                if (value != null)
                {
                    if (value.GetType() == typeof(Boolean))
                    {
                        objAttItem.Val_Named = value.ToString();
                        objAttItem.Val_Bit = (bool)value;
                    }
                    else
                    {
                        bool boolVal;

                        if (bool.TryParse(value.ToString(), out boolVal))
                        {
                            objAttItem.Val_Named = boolVal.ToString();
                            objAttItem.Val_Bit = boolVal;
                        }
                        else
                        {
                            objAttItem = null;
                        }
                    }
                }
                else
                {
                    if (doStandardValue)
                    {
                        objAttItem.Val_Named = false.ToString();
                        objAttItem.Val_Bit = false;
                    }
                    else
                    {
                        objAttItem = null;
                    }
                }
            }
            else if (oItemAttributeType.GUID_Parent == dataTypes.DType_DateTime.GUID)
            {
                if (value.GetType() != null)
                {
                    if (value.GetType() == typeof(DateTime))
                    {
                        objAttItem.Val_Named = value.ToString();
                        objAttItem.Val_Date = (DateTime)value;
                    }
                    else
                    {
                        DateTime dateVal;
                        if (DateTime.TryParse(value.ToString(), out dateVal))
                        {
                            objAttItem.Val_Named = dateVal.ToString();
                            objAttItem.Val_Date = dateVal;
                        }
                        else
                        {
                            objAttItem = null;
                        }
                    }
                }
                else
                {
                    if (doStandardValue)
                    {
                        objAttItem.Val_Named = DateTime.Now.ToString();
                        objAttItem.Val_Date = DateTime.Now;
                    }
                    else
                    {
                        objAttItem = null;
                    }
                }
            }
            else if (oItemAttributeType.GUID_Parent == dataTypes.DType_Int.GUID)
            {
                if (value.GetType() != null)
                {
                    if (value.GetType() == typeof(long) || value.GetType() == typeof(int) || value.GetType() == typeof(Int64) || value.GetType() == typeof(Int32))
                    {
                        objAttItem.Val_Named = value.ToString();
                        objAttItem.Val_Int = (long)value;
                    }
                    else
                    {
                        long intVal;
                        if (long.TryParse(value.ToString(), out intVal))
                        {
                            objAttItem.Val_Named = intVal.ToString();
                            objAttItem.Val_Int = intVal;
                        }
                        else
                        {
                            objAttItem = null;
                        }
                    }
                }
                else
                {
                    if (doStandardValue)
                    {
                        objAttItem.Val_Named = 0.ToString();
                        objAttItem.Val_Int = 0;
                    }
                    else
                    {
                        objAttItem = null;
                    }
                }
            }
            else if (oItemAttributeType.GUID_Parent == dataTypes.DType_Real.GUID)
            {
                if (value.GetType() != null)
                {
                    if (value.GetType() == typeof(double))
                    {
                        objAttItem.Val_Named = value.ToString();
                        objAttItem.Val_Double = (double)value;
                    }
                    else
                    {
                        double dblVal;
                        if (double.TryParse(value.ToString(), out dblVal))
                        {
                            objAttItem.Val_Named = dblVal.ToString();
                            objAttItem.Val_Real = dblVal;
                        }
                        else
                        {
                            objAttItem = null;
                        }
                    }
                }
                else
                {
                    if (doStandardValue)
                    {
                        objAttItem.Val_Named = "0.0";
                        objAttItem.Val_Real = 0.0;
                    }
                    else
                    {
                        objAttItem = null;
                    }
                }
            }
            else if (oItemAttributeType.GUID_Parent == dataTypes.DType_String.GUID)
            {
                if (value.GetType() != null)
                {
                    objAttItem.Val_Named = value.ToString().Length > 255 ? value.ToString().Substring(0, 255) : value.ToString();
                    objAttItem.Val_String = value.ToString();
                    
                }
                else
                {
                    if (doStandardValue)
                    {
                        objAttItem.Val_Named = "";
                        objAttItem.Val_String = "";
                    }
                    else
                    {
                        objAttItem = null;
                    }
                }
            }

            if (objAttItem != null)
            {
                orderId = 0;
                if (getNextOrderId)
                {
                    orderId = dbConnector_OrderId.GetDataAttOrderId(oItemObject, oItemAttributeType, false);
                }
                orderId++;
                objAttItem.OrderID = orderId;
            }

            return objAttItem;
        }

        public clsObjectRel Rel_ObjectRelation(clsOntologyItem oItemLeft, 
            clsOntologyItem oItemRight, 
            clsOntologyItem oItemRelationType, 
            bool getNextOrderId = false, 
            long orderId = 1, 
            bool full = false)
        {

            try
            {
                var relItem = new clsObjectRel 
                {
                    ID_Object = oItemLeft.GUID, 
                    Name_Object = full ? oItemLeft.Name : null, 
                    ID_Parent_Object = oItemLeft.GUID_Parent,
                    ID_Other = oItemRight.GUID,
                    Name_Other = full ? oItemRight.Name : null,
                    ID_Parent_Other = oItemRight.GUID_Parent,
                    ID_RelationType = oItemRelationType.GUID,
                    Name_RelationType = full ? oItemRelationType.Name : null,
                    OrderID = orderId,
                    Ontology = oItemRight.Type
                };

                if (oItemLeft.GUID_Parent == null || oItemLeft.Type == null)
                {
                    return null;
                }
                else
                {
                    return relItem;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            
        }

        public clsClassRel Rel_ClassRelation(clsOntologyItem oItemClassLeft, 
            clsOntologyItem oItemClassRight,
            clsOntologyItem oItemRelationType,
            long minForw = 0,
            long maxForw = -1,
            long maxBackw = -1)
        {
            clsClassRel relClassRel = null;

            try
            {
                if ((maxForw == -1 && minForw >= 0) || (maxForw > 0 && minForw >= 0 && minForw < maxForw))
                {
                    if (maxBackw > 0 || maxBackw == -1)
                    {
                        if (oItemClassLeft != null && oItemClassRight != null && oItemRelationType != null)
                        {
                            relClassRel = new clsClassRel
                            {
                                ID_Class_Left = oItemClassLeft.GUID,
                                ID_Class_Right = oItemClassRight.GUID,
                                ID_RelationType = oItemRelationType.GUID,
                                Min_Forw = minForw,
                                Max_Forw = maxForw,
                                Max_Backw = maxBackw,
                                Ontology = globals.Type_Class
                            };

                            if (string.IsNullOrEmpty(relClassRel.ID_Class_Left) ||
                                string.IsNullOrEmpty(relClassRel.ID_Class_Right) ||
                                string.IsNullOrEmpty(relClassRel.ID_RelationType) ||
                                relClassRel.Min_Forw == null ||
                                relClassRel.Max_Forw == null ||
                                relClassRel.Max_Backw == null)
                            {
                                relClassRel = null;
                            }
                        }
                        else if (oItemClassLeft != null && oItemRelationType != null)
                        {
                            relClassRel = new clsClassRel
                            {
                                ID_Class_Left = oItemClassLeft.GUID,
                                ID_RelationType = oItemRelationType.GUID,
                                Min_Forw = minForw,
                                Max_Forw = maxForw,
                                Max_Backw = maxBackw,
                                Ontology = globals.Type_Class
                            };

                            if (string.IsNullOrEmpty(relClassRel.ID_Class_Left) ||
                                string.IsNullOrEmpty(relClassRel.ID_RelationType) ||
                                relClassRel.Min_Forw == null ||
                                relClassRel.Max_Forw == null ||
                                relClassRel.Max_Backw == null)
                            {
                                relClassRel = null;
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                relClassRel = null;
            }

            return relClassRel;
            
        }

        public clsRelationConfig(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }

        public void Initialize()
        {
            dbConnector_OrderId = new OntologyModDBConnector(globals);
        }
    }
}
