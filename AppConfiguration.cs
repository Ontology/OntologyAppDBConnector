﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OntologyAppDBConnector
{
    [Flags]
    public enum ConfigFileSection
    {
        DocumentElement = 0,
        dtbl_BaseConfig = 1,
        ConfigItem_Name = 2,
        ConfigItem_Value = 4
    }
    public static class AppConfiguration
    {
        public static string Server { get; set; }
        public static string Index { get; set; }
        public static int Port { get; set; }
        public static string RepServer { get; set; }
        public static string RepDatabase { get; set; }
        public static string RepInstance { get; set; }
        public static string RepIndex { get; set; }
        public static int SearchRange { get; set; }
        public static string WebSocketServer { get; set; }
        public static int WebSocketPort { get; set; }

        private static object readLocker = new object();

        public static string GetValue(string name)
        {
            var configItem = ConfigItems.FirstOrDefault(ci => ci.ConfigItem.ToLower() == name.ToLower());

            if (configItem != null)
            {
                return configItem.ConfigValue;
            }
            else
            {
                return null;
            }
        }

        private static string moduleSearchPath;

        public static string ModuleSearchPath
        {
            get { return moduleSearchPath; }
            set
            {
                moduleSearchPath = value;

                if (moduleSearchPath != null)
                {
                    moduleSearchPath = Environment.ExpandEnvironmentVariables(moduleSearchPath);
                }
                    
            }
        }

        private static string _configPath;
        public static List<AppConfigItem> ConfigItems { get; set; }

        private static string GetPathOfConfigFile()
        {
            if (!string.IsNullOrEmpty(_configPath))
            {
                _configPath = Environment.ExpandEnvironmentVariables(_configPath);    
            }
            
            if (string.IsNullOrEmpty(_configPath))
            {
                _configPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                _configPath += Path.DirectorySeparatorChar + @"Config\Config_ont.xml";
            }
            return _configPath;
        }

        public static bool ReadConfig(string configPath = null)
        {
            _configPath = configPath;
            var xmlDocument = new XmlDocument();
            var result = true;
            _configPath = GetPathOfConfigFile();
            lock (readLocker)
            {
                if (ConfigItems == null)
                {
                    ConfigItems = new List<AppConfigItem>();
                }
                else
                {
                    ConfigItems.Clear();
                }
                if (File.Exists(_configPath))
                {
                    try
                    {
                    
                        xmlDocument.Load(_configPath);
                        var xmlElements = xmlDocument.GetElementsByTagName("dtbl_BaseConfig");

                        foreach (XmlElement xmlElement in xmlElements)
                        {
                            var nameNode = xmlElement.ChildNodes[0];
                            var valueNode = xmlElement.ChildNodes[1];

                            var name = nameNode.InnerText;
                            var value = valueNode.InnerText;

                            var configItem = new AppConfigItem
                            {
                                ConfigItem = name,
                                ConfigValue = value
                            };
                            configItem.CreateOriginal();
                            ConfigItems.Add(configItem);
                        }
                    
                    

                        result = GetCurrentConfiguration();

                    }
                    catch (Exception)
                    {
                        result = false;

                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool WriteConfig()
        {
            if (File.Exists(_configPath))
            {
                try
                {
                    TextWriter textWriter;
                    using (textWriter = new StreamWriter(_configPath))
                    {
                        XmlTextWriter xmlTextWriter;
                        using (xmlTextWriter = new XmlTextWriter(textWriter))
                        {
                            xmlTextWriter.WriteStartDocument();
                            xmlTextWriter.WriteStartElement(Enum.GetName(typeof(ConfigFileSection), ConfigFileSection.DocumentElement));

                            foreach (var configItem in ConfigItems)
                            {
                                xmlTextWriter.WriteStartElement(Enum.GetName(typeof(ConfigFileSection), ConfigFileSection.dtbl_BaseConfig));

                                xmlTextWriter.WriteStartElement(Enum.GetName(typeof(ConfigFileSection), ConfigFileSection.ConfigItem_Name));
                                xmlTextWriter.WriteValue(configItem.ConfigItem);

                                xmlTextWriter.WriteEndElement();

                                xmlTextWriter.WriteStartElement(Enum.GetName(typeof(ConfigFileSection), ConfigFileSection.ConfigItem_Value));
                                xmlTextWriter.WriteValue(configItem.ConfigValue);

                                xmlTextWriter.WriteEndElement();

                                xmlTextWriter.WriteEndElement();
                            }

                            xmlTextWriter.WriteEndElement();
                        }
                    }
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
                
                

            }
            else
            {
                return false;
            }
        }

        public static int? GetConfigIntValue(string configItemName)
        {
            if (ConfigItems != null && ConfigItems.Any())
            {
                var configItem =
                    ConfigItems.FirstOrDefault(ci => ci.ConfigItem.ToLower() == configItemName.ToLower());

                if (configItem != null)
                {
                    var value = configItem.ConfigValue;
                    int intVal;
                    if (int.TryParse(value, out intVal))
                    {
                        return intVal;
                    }
                    else
                    {
                        return null;
                    }
                    
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static string GetConfigStringValue(string configItemName)
        {
            if (ConfigItems != null && ConfigItems.Any())
            {
                var configItem =
                    ConfigItems.FirstOrDefault(ci => ci.ConfigItem.ToLower() == configItemName.ToLower());

                if (configItem != null)
                {
                    return configItem.ConfigValue;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            
        }

        private static bool GetCurrentConfiguration()
        {
            Index = GetConfigStringValue("Index");
            Port = 0;
            int? port = GetConfigIntValue("Port");
            if (port != null)
            {
                Port = port.Value;
            }
            Server = GetConfigStringValue("Server");
            RepServer = GetConfigStringValue("server_report");
            RepInstance = GetConfigStringValue("server_instance");
            RepDatabase = GetConfigStringValue("database");
            RepIndex = GetConfigStringValue("ReportIndex");
            ModuleSearchPath = GetConfigStringValue("ModuleSearchPath");
            WebSocketServer = GetConfigStringValue("WebsocketServer");
            var webSocketPort = GetConfigIntValue("WebsocketPort");
            if (webSocketPort != null)
            {
                WebSocketPort = webSocketPort.Value;
            }
            SearchRange = 10000;
            int? searchRange = GetConfigIntValue("SearchRange");
            if (searchRange != null)
            {
                SearchRange = searchRange.Value;
            }

            return Index != null && Port != 0 && Server != null && RepServer != null && RepInstance != null && RepDatabase != null && RepIndex != null && ModuleSearchPath != null;
        }
    }
}
