﻿using System;
using System.Collections.Generic;
using System.Linq;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;

namespace OntologyAppDBConnector
{
    public class clsTransaction
    {
        private List<clsTransactionItem> objOList_Item = new List<clsTransactionItem>();
        private clsTransactionItem objOItem_TransItem = new clsTransactionItem();
        private OntologyModDBConnector dbConnector;
        private clsLogStates logStates = new clsLogStates();
        private clsClassTypes classTypes = new clsClassTypes();
        private clsTypes types = new clsTypes();

        public clsTransactionItem OItem_Last
        {
            get { return objOList_Item.Last(); }
        }

        public clsOntologyItem del_ObjectAndRelations(clsOntologyItem OItem_Object)
        {
            var objOList_AttributesDel = new List<clsObjectAtt> {new clsObjectAtt {ID_Object = OItem_Object.GUID}};
            var objOList_ObjectsForw = new List<clsObjectRel> {new clsObjectRel {ID_Object = OItem_Object.GUID}};
            var objOList_ObjectsBackw = new List<clsObjectRel> {new clsObjectRel {ID_Other = OItem_Object.GUID}};

            var objOItem_Result = dbConnector.DelObjectAtts(objOList_AttributesDel);
            
            if (objOItem_Result.GUID == logStates.LogState_Error.GUID)
            {
                objOItem_Result = dbConnector.DelObjectRels(objOList_ObjectsForw);
                if (objOItem_Result.GUID != logStates.LogState_Error.GUID)
                {
                    objOItem_Result = dbConnector.DelObjectRels(objOList_ObjectsBackw);
                        
                    if (objOItem_Result.GUID != logStates.LogState_Error.GUID)
                    {
                        objOItem_Result = dbConnector.DelObjects((new List<clsOntologyItem> {OItem_Object}));
                    }
                }
            }
            
            return objOItem_Result;
        }
        
        public clsOntologyItem do_Transaction(object OItem_Item, bool boolRemoveAll = false, bool boolRemoveItem = false)
        {
            clsOntologyItem objOItem_Result = logStates.LogState_Error;
            List<clsOntologyItem> objOL_Items = new List<clsOntologyItem>();
            List<clsObjectAtt> objOL_AItems = new List<clsObjectAtt>();
            List<clsObjectRel> objOL_RItems = new List<clsObjectRel>();
            List<clsClassAtt> objOL_CLaItems = new List<clsClassAtt>();
            List<clsClassRel> objOL_ClrItems = new List<clsClassRel>();

            objOItem_TransItem = new clsTransactionItem();

            objOItem_TransItem.Removed = boolRemoveItem;

            if (OItem_Item.GetType().Name == classTypes.ClassType_ObjectAtt)
            {
                objOItem_TransItem.OItemObjectAtt = (clsObjectAtt)OItem_Item;
                objOItem_Result = clear_Relations(OItem_Item.GetType().Name, boolRemoveAll:boolRemoveAll);
                if (boolRemoveItem == false)
                {
                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                    {

                        objOL_AItems.Add((clsObjectAtt)OItem_Item);
                        objOItem_Result = dbConnector.SaveObjAtt(objOL_AItems);
                        if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                        {
                            objOItem_TransItem.OItemObjectAtt.ID_Attribute =
                                dbConnector.SavedObjectAtts.First().ID_Attribute;
                        }
                    }

                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ObjectRel)       
            {


                objOItem_TransItem.OItemObjectRel = (clsObjectRel) OItem_Item;
                if (objOItem_TransItem.OItemObjectRel.Ontology == types.ObjectType)
                {
                    objOItem_Result = clear_Relations(OItem_Item.GetType().Name, boolRemoveAll:boolRemoveAll);
                }
                else
                {
                    objOItem_Result = clear_Relations(OItem_Item.GetType().Name, boolNeutral:true, boolRemoveAll:boolRemoveAll);
                }

                if (boolRemoveItem == false)
                {
                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                    {
                        objOL_RItems.Add((clsObjectRel)OItem_Item);

                        objOItem_Result = dbConnector.SaveObjRel(objOL_RItems);
                    }
                }


                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ClassAtt)
            {

                objOItem_TransItem.OItemClassAtt = (clsClassAtt) OItem_Item;
                objOItem_Result = clear_Relations(classTypes.ClassType_ObjectAtt, boolRemoveAll = boolRemoveAll);
                  
                if (boolRemoveItem == false)
                {
                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                    {
                        objOL_CLaItems.Add((clsClassAtt)OItem_Item);
                        objOItem_Result = dbConnector.SaveClassAtt(objOL_CLaItems);
                    }
                }


                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ClassRel)       
            {
                objOItem_TransItem.OItemClassRel = (clsClassRel)OItem_Item;
                objOItem_Result = clear_Relations(OItem_Item.GetType().Name, boolRemoveAll:boolRemoveAll);

                if (boolRemoveItem == false)
                {
                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                    {
                        objOL_ClrItems.Add((clsClassRel) OItem_Item);
                        objOItem_Result = dbConnector.SaveClassRel(objOL_ClrItems);
                    }

                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_OntologyItem)       
            {
                objOItem_TransItem.OItemOntologyItem = (clsOntologyItem)OItem_Item;
                if (boolRemoveItem == false)
                {
                    objOL_Items.Add((clsOntologyItem) OItem_Item);
                    if (objOItem_TransItem.OItemOntologyItem.Type == types.AttributeType)
                    {
                        var olAttributeTypes = new List<clsOntologyItem> { (clsOntologyItem)OItem_Item };
                        objOItem_Result = dbConnector.SaveAttributeTypes(olAttributeTypes);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.ClassType)
                    {
                        var olClasses = new List<clsOntologyItem> {(clsOntologyItem) OItem_Item};
                        objOItem_Result = dbConnector.SaveClass(olClasses);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.ObjectType)
                    {
                        objOItem_Result = dbConnector.SaveObjects(objOL_Items);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.RelationType)
                    {
                        var olRelationTypes = new List<clsOntologyItem> {(clsOntologyItem) OItem_Item};
                        objOItem_Result = dbConnector.SaveRelationTypes(olRelationTypes);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else
                    {
                        objOItem_TransItem.TransactionResult = logStates.LogState_Error;
                    }
                }
                else
                {
                    objOL_Items.Add((clsOntologyItem)OItem_Item);
                    if (objOItem_TransItem.OItemOntologyItem.Type == types.AttributeType)
                    {
                        objOItem_Result = dbConnector.DelAttributeTypes(objOL_Items);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.ClassType)
                    {
                        objOItem_Result = dbConnector.DelClasses(objOL_Items);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.ObjectType)
                    {
                        objOItem_Result = dbConnector.DelObjects(objOL_Items);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else if (objOItem_TransItem.OItemOntologyItem.Type == types.RelationType)
                    {
                        objOItem_Result = dbConnector.DelRelationTypes(objOL_Items);
                        objOItem_TransItem.TransactionResult = objOItem_Result;
                    }
                    else
                    {
                        objOItem_TransItem.TransactionResult = logStates.LogState_Error;
                    }
                    
                }



            }

            objOList_Item.Add(objOItem_TransItem);

            return objOItem_Result;
        }

        public clsOntologyItem fill_TransactionList(Object OItem_Item, bool boolRemoveAll = false)
        {
            var objOItem_Result = logStates.LogState_Error;
            var objOL_Items = new List<clsOntologyItem>();
            var objOL_AItems = new  List<clsObjectAtt>();
            var objOL_RItems = new List<clsObjectRel>();
            var objOL_CLaItems = new List<clsClassAtt>();
            var objOL_ClrItems = new List<clsClassRel>();

            objOItem_TransItem = new clsTransactionItem();

            if (OItem_Item.GetType().Name == classTypes.ClassType_ObjectAtt)
            {
                objOItem_TransItem.OItemObjectAtt = (clsObjectAtt) OItem_Item;

                if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                {
                    objOL_AItems.Add((clsObjectAtt)OItem_Item);

                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ObjectRel)
            {
                objOItem_TransItem.OItemObjectRel = (clsObjectRel) OItem_Item;

                if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                {
                    objOL_RItems.Add((clsObjectRel)OItem_Item);
                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ClassAtt)
            {
                objOItem_TransItem.OItemClassAtt = (clsClassAtt) OItem_Item;

                if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                {
                    objOL_CLaItems.Add((clsClassAtt) OItem_Item);

                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_ClassRel)
            {
                objOItem_TransItem.OItemClassRel = (clsClassRel)OItem_Item;

                if (objOItem_Result.GUID == logStates.LogState_Success.GUID)
                {
                    objOL_ClrItems.Add((clsClassRel)OItem_Item);

                }

                objOItem_TransItem.TransactionResult = objOItem_Result;
            }
            else if (OItem_Item.GetType().Name == classTypes.ClassType_OntologyItem)
            {
                   objOItem_TransItem.OItemOntologyItem = (clsOntologyItem)OItem_Item;
                    objOL_Items.Add((clsOntologyItem)OItem_Item);
            }

            objOList_Item.Add(objOItem_TransItem);

            return objOItem_Result;
        }
        
        public clsOntologyItem rollback()
        {
            var objOItem_Result = logStates.LogState_Error;

            clsTransactionItem objTransactionItem;

            if (objOList_Item.Any())
            {
                for (var i = objOList_Item.Count - 1; i >= 0;i--)
                {
                    objTransactionItem = objOList_Item[i];
                    objOItem_Result = rollback_One(objTransactionItem);
                    if (objOItem_Result.GUID == logStates.LogState_Error.GUID)
                    {
                        break;
                    }
                }
            }
            else
            {
                objOItem_Result = logStates.LogState_Error;
            }

            return objOItem_Result;
        }
        
        private clsOntologyItem rollback_One(clsTransactionItem objTransactionItem)
        {
            clsOntologyItem objOItem_Result;
            var objOItem_Class = new clsOntologyItem();
            var objOItem_AttributeType = new clsOntologyItem();
            var objOLClassAtt = new List<clsClassAtt>();
            var objOLClassRel = new List<clsClassRel>();
            var objOLObjAtt = new List<clsObjectAtt>();
            var objOLObjRel = new List<clsObjectRel>();
            var objOLOntologyItem = new List<clsOntologyItem>();
            var objOLObjAttSaved = new List<clsObjectAtt>();

            if (objTransactionItem.SavedType == classTypes.ClassType_ClassAtt)
            {
                objOItem_Class.GUID = objTransactionItem.OItemClassAtt.ID_Class;
                objOItem_AttributeType.GUID = objTransactionItem.OItemClassAtt.ID_AttributeType;

                if (objTransactionItem.Removed == false)
                {
                    objOItem_Result = dbConnector.DelClassAttType(objOItem_Class,
                                                              objOItem_AttributeType);
                }
                else
                {
                    objOLClassAtt.Add(objTransactionItem.OItemClassAtt);
                    objOItem_Result = dbConnector.SaveClassAtt(objOLClassAtt);

                }
            }
            else if (objTransactionItem.SavedType == classTypes.ClassType_ClassRel)
            {
                if (objTransactionItem.Removed == false)
                {
                    objOLClassRel.Add(new clsClassRel
                    {
                        ID_Class_Left = objTransactionItem.OItemClassRel.ID_Class_Left,
                        ID_Class_Right = objTransactionItem.OItemClassRel.ID_Class_Right,
                        ID_RelationType = objTransactionItem.OItemClassRel.ID_RelationType
                    });

                    if (dbConnector.DelClassRel(objOLClassRel).Count>0)
                    {
                        objOItem_Result = logStates.LogState_Success;
                    }
                    else
                    {
                        objOItem_Result = logStates.LogState_Error;
                    }
                }
                else
                {
                    objOLClassRel.Add(objTransactionItem.OItemClassRel);

                    objOItem_Result = dbConnector.SaveClassRel(objOLClassRel);
                }
            }
            else if (objTransactionItem.SavedType == classTypes.ClassType_ObjectAtt)
            {
                if (objTransactionItem.Removed == false)
                {
                    if (objTransactionItem.OItemObjectAtt.ID_Attribute != null)
                    {
                        objOLObjAtt.Add(new clsObjectAtt { ID_Attribute = objTransactionItem.OItemObjectAtt.ID_Attribute });


                        objOItem_Result = dbConnector.DelObjectAtts(objOLObjAtt);
                    }
                    else
                    {
                        objOItem_Result = logStates.LogState_Success;
                    }
                }
                else
                {
                    objOLObjAtt.Add(objTransactionItem.OItemObjectAtt);

                    objOItem_Result = dbConnector.SaveObjAtt(objOLObjAtt);

                }
            }
            else if (objTransactionItem.SavedType == classTypes.ClassType_ObjectRel)
            {
                if (objTransactionItem.Removed == false)
                {
                    objOLObjRel.Add(new clsObjectRel
                    {
                        ID_Object = objTransactionItem.OItemObjectRel.ID_Object,
                        ID_Other = objTransactionItem.OItemObjectRel.ID_Other,
                        ID_RelationType = objTransactionItem.OItemObjectRel.ID_RelationType
                    });

                    objOItem_Result = dbConnector.DelObjectRels(objOLObjRel);
                }
                else
                {
                    objOLObjRel.Add(objTransactionItem.OItemObjectRel);

                    objOItem_Result = dbConnector.SaveObjRel(objOLObjRel);
                }
            }
            else if (objTransactionItem.SavedType == classTypes.ClassType_OntologyItem)
            {
                objOLOntologyItem.Add(objTransactionItem.OItemOntologyItem);
                if (objTransactionItem.Removed == false)
                {
                    objOItem_Result = dbConnector.DelObjects(objOLOntologyItem);
                }
                else
                {
                    objOItem_Result = dbConnector.SaveObjects(objOLOntologyItem);
                }
            }
            else
            {
                objOItem_Result = logStates.LogState_Error;
            }

            return objOItem_Result;
        }
        
        private clsOntologyItem clear_Relations(string strType, bool boolNeutral = false, bool boolRemoveAll = false)
        {
            var objOItem_Result = logStates.LogState_Error;
            clsOntologyItem objOItem_Result_Search;
            clsOntologyItem objOItem_Result_Del;

            var objOL_ObjAtt = new List<clsObjectAtt>();
            var objOL_ObjAtt_Search = new List<clsObjectAtt>();
            var objOL_ObjAtt_Del = new List<clsObjectAtt>();
            var objOL_ObjRel = new List<clsObjectRel>();
            var objOL_ObjRel_Search = new List<clsObjectRel>();
            var objOL_ObjRel_Del = new List<clsObjectRel>();
            var objOL_ClassRel_Del = new List<clsClassRel>();

            if (strType == classTypes.ClassType_ObjectAtt)
            {
                if (boolRemoveAll)
                {
                    objOL_ObjAtt_Del.Add(new clsObjectAtt
                    {
                        ID_Object = objOItem_TransItem.OItemObjectAtt.ID_Object,
                        ID_AttributeType = objOItem_TransItem.OItemObjectAtt.ID_AttributeType
                    });
                    objOItem_Result = dbConnector.GetDataObjectAtt(objOL_ObjAtt_Del);

                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjAtts.Any())
                    {
                        objOItem_Result = dbConnector.DelObjectAtts(dbConnector.ObjAtts);
                    }
                    
                }   
                else
                {
                    if (objOItem_TransItem.OItemObjectAtt.ID_Attribute != null &&
                        !string.IsNullOrEmpty(objOItem_TransItem.OItemObjectAtt.ID_Attribute))
                    {
                        objOL_ObjAtt_Del.Add(new clsObjectAtt { ID_Attribute = objOItem_TransItem.OItemObjectAtt.ID_Attribute });

                        objOItem_Result = dbConnector.DelObjectAtts(objOL_ObjAtt_Del);
                    }
                    else
                    {
                        objOItem_Result = logStates.LogState_Success;
                    }

                }
            }
            else if (strType == classTypes.ClassType_ObjectRel)
            {
                if (boolRemoveAll)
                {

                    if (!boolNeutral)
                    {
                        objOL_ObjRel_Del.Add(new clsObjectRel
                        {
                            ID_Object = objOItem_TransItem.OItemObjectRel.ID_Object,
                            ID_Parent_Other = objOItem_TransItem.OItemObjectRel.ID_Parent_Other,
                            ID_RelationType = objOItem_TransItem.OItemObjectRel.ID_RelationType
                        });
                    }
                    else
                    {
                        objOL_ObjRel_Del.Add(new clsObjectRel
                        {
                            ID_Object = objOItem_TransItem.OItemObjectRel.ID_Object,
                            ID_RelationType = objOItem_TransItem.OItemObjectRel.ID_RelationType
                        });
                    }
                    objOItem_Result = dbConnector.GetDataObjectRel(objOL_ObjRel_Del);
                    if (objOItem_Result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
                    {
                        objOItem_Result = dbConnector.DelObjectRels(dbConnector.ObjectRels);
                    }
                }   
                else
                {
                    objOL_ObjRel_Del.Add(new clsObjectRel
                    {
                        ID_Object = objOItem_TransItem.OItemObjectRel.ID_Object,
                        ID_Other = objOItem_TransItem.OItemObjectRel.ID_Other,
                        ID_RelationType = objOItem_TransItem.OItemObjectRel.ID_RelationType
                    });
                    objOItem_Result = dbConnector.DelObjectRels(objOL_ObjRel_Del);
                }
            }
            else if (strType == classTypes.ClassType_ClassRel)
            {
                if (objOItem_TransItem.OItemClassRel.ID_Class_Left != null &&
                    objOItem_TransItem.OItemClassRel.ID_RelationType != null)
                {
                    objOL_ClassRel_Del.Add(new clsClassRel {ID_Class_Left = objOItem_TransItem.OItemClassRel.ID_Class_Left,
                                                             ID_Class_Right = objOItem_TransItem.OItemClassRel.ID_Class_Right,
                                                             ID_RelationType = objOItem_TransItem.OItemClassRel.ID_RelationType});


                    objOItem_Result = dbConnector.DelClassRel(objOL_ClassRel_Del);
                }
                else
                {
                    objOItem_Result = logStates.LogState_Error;
                }
            }

            if (objOItem_Result.GUID == logStates.LogState_Nothing.GUID)
            {
                objOItem_Result = logStates.LogState_Success;
            }

            return objOItem_Result;
        }
        
        public void ClearItems()
        {
            objOList_Item.Clear();
        }

        public clsTransaction(Globals globals)
        {
            dbConnector = new OntologyModDBConnector(globals);
            objOList_Item = new List<clsTransactionItem>();
        }

    
    }
}
