﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector
{
    public static class XMLEntries
    {
        public static string Items { get { return "Items"; } }
        public static string Item { get { return "Item"; } }
        public static string Type { get { return "Type"; } }
        public static string Id { get { return "Id"; } }
        public static string Id_Parent { get { return "Id_Parent"; } }
        public static string Id_AttributeType { get { return "Id_AttributeType"; } }
        public static string Id_Class { get { return "Id_Class"; } }
        public static string Id_DataType { get { return "Id_DataType"; } }
        public static string Min { get { return "Min"; } }
        public static string Max { get { return "Max"; } }
        public static string Id_Class_Left { get { return "Id_Class_Left"; } }
        public static string Id_Class_Right { get { return "Id_Class_Right"; } }
        public static string Id_RelationType { get { return "Id_RelationType"; } }
        public static string Min_Forw { get { return "Min_Forw"; } }
        public static string Max_Forw { get { return "Max_Forw"; } }
        public static string Max_Backw { get { return "Max_Backw"; } }
        public static string Ontology { get { return "Ontology"; } }
        public static string Id_Attribute { get { return "Id_Attribute"; } }
        public static string Id_Object { get { return "Id_Object"; } }
        public static string OrderId { get { return "OrderId"; } }
        public static string Val_Named { get { return "Val_Named"; } }
        public static string Val_Bit { get { return "Val_Bit"; } }
        public static string Val_Date { get { return "Val_Date"; } }
        public static string Val_Double { get { return "Val_Double"; } }
        public static string Val_Lng { get { return "Val_Lng"; } }
        public static string Val_String { get { return "Val_String"; } }
        public static string Id_Parent_Object { get { return "Id_Parent_Object"; } }
        public static string Id_Other { get { return "Id_Other"; } }
        public static string Id_Parent_Other { get { return "Id_Parent_Other"; } }
    }
}

