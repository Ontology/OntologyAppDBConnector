﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector
{
    public class ModuleFunction
    {
        public string GUIDFunction { get; set; }
        public string NameFunction { get; set; }
        public string GUIDDestOntology { get; set; }
    }
}
