﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Hosting;
using System.Text;
using System.Threading.Tasks;
using Nest;
using OntologyClasses.BaseClasses;

namespace OntologyAppDBConnector
{
    public static class ArgumentParsing
    {
        private static List<string> _arguments;
        private static List<string> _parsedArguments;

        private static Globals _globals;

        private static OntologyModDBConnector _dbLevel;

        private static string _external;

        public static string Session { get; set; }

        private static List<clsOntologyItem> _oListItems;

        private static List<ModuleFunction> _functionList;

        private static List<string> _unparsedList;

        public static List<clsOntologyItem> Items
        {
            get { return _oListItems; }
        }

        public static List<ModuleFunction> FunctionList
        {
            get { return _functionList; }
        }

        public static List<string> UnparsedArguments
        {
            get { return _unparsedList; }
        }

        public static string External
        {
            get { return _external; }
        }

        public static void ReParseArguments(Globals globals, List<string> arguments )
        {
            _globals = globals;

            _arguments = arguments;

            _parsedArguments = new List<string>();

            _dbLevel = new OntologyModDBConnector(_globals);

            SetExternal(arguments);

            _oListItems = _arguments.Select(arg => GetOItem(arg)).Where(obj => obj != null).ToList();
            _functionList = new List<ModuleFunction>();
            _arguments.ForEach(arg =>
            {
                _functionList.AddRange(GetModuleFunction(arg));
            });

            Session = "";

            _arguments.ForEach(arg => GetSession(arg));

            _unparsedList = (from arg in arguments
                             join parsedArgument in _parsedArguments on arg equals parsedArgument into parsedArguments
                             from parsedArgument in parsedArguments.DefaultIfEmpty()
                             select arg).ToList();
        }

        public static void ParseArguments(Globals globals, List<string> arguments )
        {
            if (_globals == null)
            {
                _globals = globals;

                _arguments = arguments;

                _parsedArguments = new List<string>();

                _dbLevel = new OntologyModDBConnector(_globals);
            }

            SetExternal(arguments);

            _oListItems = _arguments.Select(arg => GetOItem(arg)).Where(obj => obj != null).ToList();
            _functionList = new List<ModuleFunction>();
            _arguments.ForEach(arg =>
            {
                _functionList.AddRange(GetModuleFunction(arg));
            });

            Session = "";

            _arguments.ForEach(arg => GetSession(arg));

            _unparsedList = (from arg in arguments
                join parsedArgument in _parsedArguments on arg equals parsedArgument into parsedArguments
                from parsedArgument in parsedArguments.DefaultIfEmpty()
                select arg).ToList();

        }

        private static clsOntologyItem GetSession(string argument)
        {
            var oItemResult = _globals.LState_Success.Clone();

            argument = argument.Trim();

            if (argument.ToLower().StartsWith("session="))
            {
                var session = argument.Substring("session=".Length);
                if (_globals.is_GUID(session))
                {
                    _parsedArguments.Add(argument);
                    Session = session;
                }
            }

            return oItemResult;
        }

        private static clsOntologyItem GetOItem(string argument)
        {
            argument = argument.Trim();

            if (argument.ToLower().StartsWith("item="))
            {
                var item = argument.Substring("item=".Length);
                var itemAndType = item.Split(',').ToList();

                if (itemAndType.Count == 2)
                {
                    var guid = itemAndType[0];
                    var type = itemAndType[1];

                    if (_globals.is_GUID(guid))
                    {
                        if (type.ToLower() == _globals.Type_AttributeType.ToLower() ||
                            type.ToLower() == _globals.Type_Class.ToLower() ||
                            type.ToLower() == _globals.Type_Object.ToLower() ||
                            type.ToLower() == _globals.Type_RelationType.ToLower())
                        {
                            var oItem = _dbLevel.GetOItem(guid, type);
                            _parsedArguments.Add(argument);
                            return oItem;
                        }
                    }

                }
            }

            return null;
        }

        private static List<ModuleFunction> GetModuleFunction(string argument)
        {
            argument = argument.Trim();

            _functionList = new List<ModuleFunction>();

            if (argument.ToLower().StartsWith("function="))
            {
                var moduleFunction = argument.Substring("function=".Length);
                var moduleFunctions = moduleFunction.Split(',').ToList();

                moduleFunctions.ForEach(moduleFunc =>
                {
                    var ontologyFunctions = moduleFunc.Split(':');

                    if (ontologyFunctions.Count() == 1 ||
                        (ontologyFunctions.Count() == 2 && _globals.is_GUID(ontologyFunctions[0])))
                    {
                        if (ontologyFunctions.Count() == 1)
                        {
                            _functionList.Add(new ModuleFunction
                            {
                                GUIDFunction = _globals.is_GUID(ontologyFunctions[0]) ? ontologyFunctions[0] : null,
                                NameFunction = !_globals.is_GUID(ontologyFunctions[0]) ? ontologyFunctions[0] : null
                            });
                        }
                        else
                        {
                            _functionList.Add(new ModuleFunction
                            {
                                GUIDDestOntology = ontologyFunctions[0],
                                GUIDFunction = _globals.is_GUID(ontologyFunctions[1]) ? ontologyFunctions[1] : null,
                                NameFunction = !_globals.is_GUID(ontologyFunctions[1]) ? ontologyFunctions[1] : null
                            });
                        }
                    }
                });

                if (moduleFunctions.Any())
                {
                    _parsedArguments.Add(argument);
                    return _functionList;
                }
                else
                {
                    _functionList = new List<ModuleFunction>();
                    return FunctionList;
                }
            }
            _functionList = new List<ModuleFunction>();
            return FunctionList;
        }

        public static void SetExternal(List<string> arguments)
        {
            var externalList = arguments.Where(arg => arg.ToLower().Trim().StartsWith("external=")).ToList();

            _external = "";

            if (externalList.Count == 1)
            {
                _external = externalList[0].Substring("esternal=".Length);
            }
        }
    }
}
