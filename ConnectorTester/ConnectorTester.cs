﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;

namespace ConnectorTester
{
    public partial class ConnectorTester : Form
    {
        private Globals globals;
        private OntologyModDBConnector ontologyModDbConnector;
        private OntologyModDBConnector ontologyModDbConnector2;
        private clsLogStates logStates = new clsLogStates();

        private void TestObjectTree()
        {
            var oItemSoftwareDevelopment = new clsOntologyItem
            {
                GUID = "71415eebce464b2cb0a2f72116b55438"
            };

            var oItemRelationType_IsSubordinated = new clsOntologyItem
            {
                GUID = "408db9f1ae424807b656729270646f0a",
                Name = "is subordinated"
            };
            var oItemResult = ontologyModDbConnector.GetDataObjectsTree(oItemSoftwareDevelopment, oItemSoftwareDevelopment, oItemRelationType_IsSubordinated);
        }

        private void TestNameSearch()
        {
            var searchObjects = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    Name = "web",
                    GUID_Parent = "f47a3e1c50b44666a41de975597c9adb"
                }
            };

            var oItemResult = ontologyModDbConnector.GetDataObjects(searchObjects);
        }
        private void TestClassRel()
        {
            var oItemResult = ontologyModDbConnector.GetDataClassRel(null, doIds: false);
        }
        private void TestClassAttributes()
        {
            var searchAttributeTypes = new List<clsClassAtt>
            {
                new clsClassAtt {ID_AttributeType = "88fb371ad44243e985a8134790b27c57"}
            };

            var oItemResult = ontologyModDbConnector.GetDataClassAtts(searchAttributeTypes);

            if (oItemResult.GUID == logStates.LogState_Success.GUID)
            {


                ontologyModDbConnector.ClassAtts.ForEach(obj =>
                {
                    textBox_Test.Text += obj.Name_AttributeType + " - " + obj.Name_Class + " - " + obj.Name_DataType + ": " + obj.Min.ToString() + " - " + obj.Max.ToString() + "\r\n";
                });

                textBox_Test.Text += "\r\n";

                var classAttFilterItem = new clsClassAtt { Min = 0, Max = 5 };

                var properties = new List<PropertyFilter>
                {
                    new PropertyFilter
                    {
                        PropertyName = "Min",
                        TestTypeOfProperty = TestType.Equal | TestType.Greater
                    },
                    new PropertyFilter
                    {
                        PropertyName = "Max",
                        TestTypeOfProperty = TestType.Equal | TestType.Smaller
                    }
                };

                var items =
                    ontologyModDbConnector.ClassAtts.Where(
                        classAtt => classAtt.PartialEquals(classAttFilterItem, properties)).ToList();

                items.ForEach(obj =>
                {
                    textBox_Test.Text += obj.Name_AttributeType + " - " + obj.Name_Class + " - " + obj.Name_DataType + ": " + obj.Min.ToString() + " - " + obj.Max.ToString() + "\r\n";
                });

                textBox_Test.Text += "\r\n";

                classAttFilterItem = new clsClassAtt { Name_Class = "^[WVE]" };
                properties = new List<PropertyFilter>
                {
                    new PropertyFilter
                    {
                        PropertyName = "Name_Class",
                        TestTypeOfProperty = TestType.Different | TestType.Regex
                    }
                };

                items =
                    ontologyModDbConnector.ClassAtts.Where(
                        classAtt => classAtt.PartialEquals(classAttFilterItem, properties)).ToList();

                items.ForEach(obj =>
                {
                    textBox_Test.Text += obj.Name_AttributeType + " - " + obj.Name_Class + " - " + obj.Name_DataType + ": " + obj.Min.ToString() + " - " + obj.Max.ToString() + "\r\n";
                });
            }
        }

        private void TestObjectAtts()
        {
            var searchClasses = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = "f30436d62ffc4071af5e3ce708b8c2d9"
                }
            };

            var result = ontologyModDbConnector.GetDataClasses(searchClasses);

            var searchObjAtts = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    Val_Int = 2
                },
                new clsObjectAtt
                {
                    Val_Int = 3
                }
            };

            result = ontologyModDbConnector.GetDataObjectAtt(searchObjAtts, doIds: false,doJoinClasses:true );

            textBox_Test.Text = string.Join("\r\n", ontologyModDbConnector.ObjAtts.Select(objAtt =>
            "AttType: " + objAtt.Name_AttributeType + " / Class: " + objAtt.Name_Class +
                       " / Object: " + objAtt.Name_Object + " / DataType: " + objAtt.Name_DataType +
                       " / Value: " + (objAtt.Val_Bit != null ? objAtt.Val_Bit.ToString() : "") +
                       " | " + (objAtt.Val_Date != null ? objAtt.Val_Date.ToString() : "") +
                       " | " + (objAtt.Val_Double != null ? objAtt.Val_Double.ToString() : "") +
                       " | " + (objAtt.Val_Int != null ? objAtt.Val_Int.ToString() : "") +
                       " | " + (objAtt.Val_String != null ? objAtt.Val_String.ToString() : "")));
            
        }

        private void TestObjects()
        {
            var searchObjects = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = "a2d7d0fe04954afe85e11dcb88a27546"
                }
            };

            var result = ontologyModDbConnector.GetDataObjects(searchObjects);

            var filterObj = new clsOntologyItem {Name = "^T"};

            var filterProperties = new List<PropertyFilter>
            {
                new PropertyFilter
                {
                    PropertyName = "Name",
                    TestTypeOfProperty = TestType.Regex
                }
            };

            var items = ontologyModDbConnector.Objects1.Where(obj => obj.PartialEquals(filterObj, filterProperties)).Select(obj => obj.Name).ToList();

            textBox_Test.Text = string.Join("\r\n", items);
        }

        public ConnectorTester()
        {
            InitializeComponent();

            globals = new Globals(true);
            ontologyModDbConnector = new OntologyModDBConnector(globals);
            ontologyModDbConnector2 = new OntologyModDBConnector(globals);

            TestObjectTree();
        }
    }
}
