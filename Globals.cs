﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System.Text;
using System.Security.Cryptography;

namespace OntologyAppDBConnector
{
    public enum ConfigItem
    {
        Index = 0,
        Port = 1,
        Server = 2,
        Server_Report = 4,
        Server_Instance = 8,
        Database = 16,
        ReportIndex = 32,
        ModuleSearchPath = 64
    }
    public class Globals
    {
        
        public Globals(string configPath, bool moduleLoad = false)
        {
            initialize(moduleLoad, configPath);
        }


        public clsDataTypes DataTypes { get; private set; }

        public clsClasses Classes { get; private set; }

        public clsBaseClassAttributes ClassAtts { get; private set; }

        public clsBaseClassRelation ClassRels { get; private set; }

        public clsRelationTypes RelationTypes { get; private set; }

        public clsAttributeTypes AttributeTypes { get; private set; }

        public clsLogStates LogStates { get; private set; }

        public clsDirections Directions { get; private set; }

        public clsVariables Variables { get; private set; }

        public clsMappingRules MappingRules { get; private set; }

        public string ServiceUrl { get; private set; }

        private clsClassTypes classTypes = new clsClassTypes();

        private clsTypes types = new clsTypes();

        private clsFields fields = new clsFields();

        public clsOntologyRelationRules OntologyRelationRules { get; private set; }

        public clsOntologyItem Class_Ontologies
        {
            get { return Classes.OItem_Class_Ontologies; }
        }

        public clsOntologyItem Class_OntologyItems
        {
            get { return Classes.OItem_Class_OntologyItems; }
        }

        public clsOntologyItem Class_OntologyItemCreationRule
        {
            get { return Classes.OItem_Class_OntologyItemCreationRule; }
        }

        public clsOntologyItem Class_OntologyRelationRule
        {
            get { return Classes.OItem_Class_OntologyRelationRule; }
        }

        public clsOntologyItem Class_OntologyJoin
        {
            get { return Classes.OItem_Class_OntologyJoin;  }
        }

        public clsOntologyItem Class_MappingRule
        {
            get { return Classes.OItem_Class_MappingRule; }
        }

        public clsOntologyItem Class_OntologyMappingItem
        {
            get { return Classes.OItem_Class_OntologyMappingItem; }
        }

        public clsOntologyItem Class_Directions
        {
            get { return Classes.OItem_Class_Directions; }
        }

        public clsOntologyItem Class_OntologyMapping
        {
            get { return Classes.OItem_Class_OntologyMappingItem; }
        }

        public clsOntologyItem Class_Module
        {
            get { return Classes.OItem_Class_Module; }
        }

        public clsOntologyItem Class_ModuleFunction
        {
            get { return Classes.OItem_Class_ModuleFunction; }
        }

        public clsOntologyItem AttributeType_Navigation
        {
            get { return AttributeTypes.OITem_AttributeType_Navigation; }
        }

        public clsOntologyItem AttributeType_OrderID
        {
            get { return AttributeTypes.OITem_AttributeType_OrderID; }
        }

        public clsOntologyItem OItem_Server
        {
            get { return objOItem_Server; }
        }

        public string WebsocketServer { get; private set; }
        public int WebsocketPort { get; private set; }

        public string Server { get; set; }
        public string Index { get; set; }
        public int Port { get; set; }

        private string strRep_Index;

        private string strRep_Server;
        private string strRep_Instance;
        private string strRep_Database;

        private int cintSearchRange = 5000;

        private string strSearchPath_Modules;


        private string strRegEx_GUID;

        private string GUID_Session;

        private clsOntologyItem objOItem_Server;
        private clsOntologyItem objOItem_WMI_ProcessorID;
        private clsOntologyItem objOItem_WMI_BaseBoardSerial;

        private clsTransaction objTransaction;

        private List<clsModuleConfig> objModuleList;

        public List<clsObjectRel> DbModuleList { get; private set; }

        public delegate  void InconsistentData();
        public event InconsistentData inconsistentData;

        public delegate void NoService();
        public event NoService noService;

        public delegate void CheckConfig();
        public event CheckConfig checkConfig;

        private OntologyModDBConnector ontologyAppDbConnector;

        public List<clsModuleConfig> ModuleList
        {
            get { return objModuleList; }
        }

        public Globals(bool ModuleLoad = true)
        {
            initialize(ModuleLoad);
        }

        public string NewGUID
        {
            get { return Guid.NewGuid().ToString().Replace("-", ""); }
        }

        public string GUIDFormat1(string strGUIDFormat2)
        {
            string strGUIDFormat1;

            strGUIDFormat1 = strGUIDFormat2.Insert(8, "-");
            strGUIDFormat1 = strGUIDFormat1.Insert(13, "-");
            strGUIDFormat1 = strGUIDFormat1.Insert(18, "-");
            strGUIDFormat1 = strGUIDFormat1.Insert(23, "-");

            return strGUIDFormat1;
        }

        public string GUIDFormat2(string strGUIDFormat1) 
        {
            return (strGUIDFormat1.Replace("-", ""));
        }


        public List<clsModuleForCommandLine> get_ModuleExecutablesInSearchPath()
        {
            var executableList = new List<clsModuleForCommandLine>();
            if (strSearchPath_Modules != null)
            {
                if (Directory.Exists(strSearchPath_Modules))
                {
                    foreach (var strFile in Directory.GetFiles(strSearchPath_Modules, "*.exe", SearchOption.AllDirectories))
                    {
                        if (Path.GetExtension(strFile).ToLower() == ".exe")
                        {
                            var strFileName = Path.GetFileName(strFile);


                            try
                            {
                                var objAssembly = Assembly.LoadFile(strFile);
                                if (objAssembly != null)
                                {
                                    if (objAssembly.GetName().Name != "vshost32")
                                    {

                                        var objGuidAttributes = objAssembly.GetCustomAttributes(typeof(GuidAttribute), true);
                                        var objVersionAttributes = objAssembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true);
                                        if (objGuidAttributes.Any() && objVersionAttributes.Any())
                                        {
                                            var strModuleGuid = ((GuidAttribute)objGuidAttributes.First()).Value.ToString().Replace("-", "");
                                            var strVersionsAll = ((AssemblyFileVersionAttribute)(objVersionAttributes.First())).Version.ToString();
                                            var strVersions = strVersionsAll.Split('.');

                                            var intMajor = 0;
                                            var intMinor = 0;
                                            var intBuild = 0;
                                            var intRevision = 0;

                                            if (strVersions.Count() == 4)
                                            {
                                                intMajor = int.Parse(strVersions[0]);
                                                intMinor = int.Parse(strVersions[1]);
                                                intBuild = int.Parse(strVersions[2]);
                                                intRevision = int.Parse(strVersions[3]);
                                            }
                                            else if (strVersions.Count() == 3)
                                            {
                                                intMajor = int.Parse(strVersions[0]);
                                                intMajor = int.Parse(strVersions[1]);
                                                intBuild = int.Parse(strVersions[2]);
                                            }
                                            else if (strVersions.Count() == 2)
                                            {
                                                intMajor = int.Parse(strVersions[0]);
                                                intMajor = int.Parse(strVersions[1]);
                                            }
                                            else if (strVersions.Count() == 1)
                                            {
                                                intMajor = int.Parse(strVersions[0]);
                                            }

                                            var objModule = new clsModuleForCommandLine
                                            {
                                                ModuleName = objAssembly.GetName().Name,
                                                ModuleGuid = strModuleGuid,
                                                Major = intMajor,
                                                Minor = intMinor,
                                                Build = intBuild,
                                                Revision = intRevision,
                                                ModulePath = strFile
                                            };
                                            executableList.Add(objModule);
                                        }


                                    }

                                }
                                objAssembly = null;
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
                else
                {
                    executableList = null;

                }
            }

            return executableList;
        }

        public string get_ConnectionStr(string strServer, string strInstance, string strDatabase)
        {
            string strConn;
            strConn = "Data Source=" + strServer;
            if (!string.IsNullOrEmpty(strInstance))
            {
                strConn = strConn + "\\" + strInstance;
            }
        
            strConn = strConn + ";Initial Catalog=" + strDatabase + ";Integrated Security=True";

            return strConn;
        }
       

        public clsOntologyItem RelationType_belongingResource 
        { get {
            return RelationTypes.OItem_RelationType_belongingResource;
        }
        }

        public  clsOntologyItem RelationType_isOfType 
            { get {
                return RelationTypes.OItem_RelationType_isOfType;
            }
        }

        public  clsOntologyItem RelationType_contains 
            { get {
                return RelationTypes.OItem_RelationType_Contains;
            }
        }

        public  clsOntologyItem RelationType_belongingAttribute 
            { get {
                return RelationTypes.OItem_RelationType_belongingAttribute;
            }
        }

        public  clsOntologyItem RelationType_belongingClass 
            { get {
                return RelationTypes.OItem_RelationType_belongingClass;
            }
        }

        public  clsOntologyItem RelationType_belongingObject 
            { get {
                return RelationTypes.OItem_RelationType_belongingObject;
            }
        }

        public  clsOntologyItem RelationType_belongingRelationType 
            { get {
                return RelationTypes.OItem_RelationType_belongingRelationType;
            }
        }

        public  clsOntologyItem RelationType_belongsTo 
            { get {
                return RelationTypes.OItem_RelationType_belongingsTo;
            }
        }

        public  clsOntologyItem RelationType_belonging 
            { get {
                return RelationTypes.OItem_RelationType_belonging;
            }
        }

        public  clsOntologyItem RelationType_Apply 
            { get {
                return RelationTypes.OItem_RelationType_Apply;
            }
        }

        public  clsOntologyItem RelationType_Dst 
            { get {
                return RelationTypes.OItem_RelationType_Dst;
            }
        }

        public  clsOntologyItem RelationType_Src 
            { get {
                return RelationTypes.OItem_RelationType_Src;
            }
        }

        public  string Rep_Server 
            { get {
                return strRep_Server;
            }
                private set { strRep_Server = value; }
        }
        public  string Rep_Instance 
            { get {
                return strRep_Instance;
            }
        }
        public  string Rep_Database 
            { get {
                return strRep_Database;
            }
        }
        public  string Session 
            { get {
                return GUID_Session;
            }
        }

        public  int SearchRange 
            { get {
                return cintSearchRange;
            }
        }

        

        public  string Index_Rep 
            { get {
                return strRep_Index;
            }
        }

        public  string Type_AttributeType 
            { get {

                return types.AttributeType;
            }
        }

        public  string Type_Class 
            { get {
                return types.ClassType;
            }
        }

        public string Type_DataType 
            { get {
                return types.DataType;
            }
        }

        public string Type_Object 
            { get {
                return types.ObjectType;
            }
        }

        public string Type_ObjectAtt 
            { get {
                return types.ObjectAtt;
            }
        }

        public string Type_ObjectRel 
            { get {
                return types.ObjectRel;
            }
        }

        public string Type_Other 
            { get {
                return types.Other;
            }
        }

        public string Type_Other_AttType 
            { get {
                return types.Other_AttType;
            }
        }

        public string Type_Other_Classes 
            { get {
                return types.Other_Classes;
            }
        }

        public string Type_Other_RelType 
            { get {
                return types.Other_RelType;
            }
        }

        public string Type_RelationType 
            { get {
                return types.RelationType;
            }
        }


        public string Type_ClassRel 
            { get {
                return types.ClassRel;
            }
        }

        public string Type_ClassAtt 
            { get {
                return types.ClassAtt;
            }
        }

        public string Field_ID_Object 
            { get {
                return fields.ID_Object;
            }
        }

        public string Field_ID_Item 
            { get {
                return fields.ID_Item;
            }
        }

        public string Field_ID_Class_Left 
            { get {
                return fields.ID_Class_Left;
            }
        }

        public string Field_ID_Class_Right 
            { get {
                return fields.ID_Class_Right;
            }
        }

        public string Field_Max_forw 
            { get {
                return fields.Max_Forw;
            }
        }

        public string Field_Min_forw 
            { get {
                return fields.Min_Forw;
            }
        }

        public string Field_Min 
            { get {
                return fields.Min;
            }
        }

        public string Field_Max 
            { get {
                return fields.Max;
            }
        }

        public string Field_Max_backw 
            { get {
                return fields.Max_Backw;
            }
        }

        public string Field_ID_AttributeType 
            { get {
                return fields.ID_AttributeType;
            }
        }

        public string Field_ID_Class 
            { get {
                return fields.ID_Class;
            }
        }

        public string Field_ID_DataType 
            { get {
                return fields.ID_DataType;
            }
        }

        public string Field_Ontology 
            { get {
                return fields.Ontology;
            }
        }

        public string Field_ID_Parent 
            { get {
                return fields.ID_Parent;
            }
        }

        public string Field_ID_Parent_Object 
            { get {
                return fields.ID_Parent_Object;
            }
        }

        public string Field_ID_Parent_Other 
            { get {
                return fields.ID_Parent_Other;
            }
        }

        public string Field_ID_RelationType 
            { get {
                return fields.ID_RelationType;
            }
        }

        public string Field_ID_Other 
            { get {
                return fields.ID_Other;
            }
        }

        public string Field_Name_AttributeType 
            { get {
                return fields.Name_AttributeType;
            }
        }

        public string Field_Name_Object 
            { get {
                return fields.Name_Object;
            }
        }

        public string Field_Name_Other 
            { get {
                return fields.Name_Other;
            }
        }

        public string Field_Name_Item 
            { get {
                return fields.Name_Item;
            }
        }

        public string Field_Name_RelationType 
            { get {
                return fields.Name_RelationType;
            }
        }

        public string Field_OrderID 
            { get {
                return fields.OrderID;
            }
        }

        public string Field_Val_Bool 
            { get {
                return fields.Val_Bool;
            }
        }

        public string Field_Val_Datetime 
            { get {
                return fields.Val_Datetime;
            }
        }

        public string Field_Val_Int 
            { get {
                return fields.Val_Int;
            }
        }

        public string Field_Val_Real 
            { get {
                return fields.Val_Real;
            }
        }

        public string Field_Val_String 
            { get {
                return fields.Val_String;
            }
        }

        public string Field_Val_Name 
            { get {
                return fields.Val_Name;
            }
        }

        public string Field_ID_Attribute 
            { get {
                return fields.ID_Attribute;
            }
        }

        public clsOntologyItem Root 
            { get {
                return Classes.OItem_Class_Root;
            }
        }

        public clsOntologyItem DType_Bool 
            { get {
                return DataTypes.DType_Bool;
            }
        }

        public clsOntologyItem  DType_DateTime 
            { get {
                return DataTypes.DType_DateTime;
            }
        }

        public clsOntologyItem  DType_Int 
            { get {
                return DataTypes.DType_Int;
            }
        }

        public clsOntologyItem  DType_Real 
            { get {
                return DataTypes.DType_Real;
            }
        }

        public clsOntologyItem  DType_String 
            { get {
                return DataTypes.DType_String;
            }
        }

        public clsOntologyItem  LState_Delete 
            { get {
                return LogStates.LogState_Delete;
            }
        }

        public clsOntologyItem  LState_Error 
            { get {
                return LogStates.LogState_Error;
            }
        }

        public clsOntologyItem  LState_Exists 
            { get {
                return LogStates.LogState_Exists;
            }
        }

        public clsOntologyItem  LState_Insert 
            { get {
                return LogStates.LogState_Insert;
            }
        }

        public clsOntologyItem  LState_Nothing 
            { get {
                return LogStates.LogState_Nothing;
            }
        }

        public clsOntologyItem  LState_Relation 
            { get {
                return LogStates.LogState_Relation;
            }
        }

        public clsOntologyItem  LState_Success 
            { get {
                return LogStates.LogState_Success;
            }
        }

        public clsOntologyItem  LState_Update 
            { get {
                return LogStates.LogState_Update;
            }
        }

        public clsOntologyItem  Direction_LeftRight 
            { get {
                return Directions.Direction_LeftRight;
            }
        }

        public clsOntologyItem  Direction_RightLeft 
            { get {
                return Directions.Direction_RightLeft;
            }
        }


        public clsOntologyItem  MappingRule_NewItem
            { get {
                return MappingRules.MappingRule_NewItem;
            }
        }

        public clsOntologyItem  MappingRule_RemoveSrc
            { get {
                return MappingRules.MappingRule_RemoveSrc;
            }
        }

        public clsOntologyItem  MappingRule_SrcItemIsDstItem
            { get {
                return MappingRules.MappingRule_SrcItemIsDstItem;
            }
        }

        public string RegexGuid
        {
            get
            {
                return strRegEx_GUID;
            }
        }

        public bool is_GUID(string strText )
        {
            var objRegExp = new Regex(strRegEx_GUID);
            if (objRegExp.IsMatch(strText) && strText != "00000000000000000000000000000000")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public clsOntologyItem GetConfigValue(string configName)
        {
            var result = LogStates.LogState_Success.Clone();
            try
            {
                var value = AppConfiguration.GetValue(configName);
                result.Additional1 = value;
            }
            catch (Exception ex)
            {
                result = LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                
            }

            return result;
        }

        public void get_ConfigData(string configPath = null)
        {
            if (AppConfiguration.ReadConfig(configPath))
            {
                Index = AppConfiguration.Index;
                Port = AppConfiguration.Port;
                Server = AppConfiguration.Server;
                Rep_Server = AppConfiguration.RepServer;
                strRep_Instance = AppConfiguration.RepInstance;
                strRep_Database = AppConfiguration.RepDatabase;
                strRep_Index = AppConfiguration.RepIndex;
                strSearchPath_Modules = AppConfiguration.ModuleSearchPath;
                WebsocketServer = AppConfiguration.WebSocketServer;
                WebsocketPort = AppConfiguration.WebSocketPort;
            }
            else
            {
                throw new Exception("Config!");
            }

            
            
            

        }

        private void LoadModules()
        {
            objModuleList = new List<clsModuleConfig>();

            try
            {
                var standardPath = Path.GetDirectoryName(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                var modulesPath = strSearchPath_Modules;

                if (string.IsNullOrEmpty(modulesPath) )
                {
                    modulesPath = standardPath;
                }

                if (Directory.Exists(modulesPath))
                {
                    foreach (var folderPath in Directory.GetDirectories(modulesPath))
                    {
                        foreach (var strFile in Directory.GetFiles(folderPath, "*.exe", SearchOption.AllDirectories))
                        {


                            try
                            {
                                var objAssembly = Assembly.LoadFile(strFile);
                                var objTypes = objAssembly.GetTypes();
                                var intModuleCount = objModuleList.Count;
                                var objModuleConfig = new clsModuleConfig { Assembly = objAssembly };

                                if (objModuleConfig.Instance != null)
                                {
                                    objModuleList.Add(objModuleConfig);
                                }

                                if (objModuleList.Count - intModuleCount > 0)
                                {
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                            }


                        }
                    }
                }
                
            }
            catch (Exception)
            {
                
                
            }
            

        }

        
        private void initialize(bool ModuleLoad, string configPath = null)
        {
            strRegEx_GUID = "[A-Za-z0-9]{8}[A-Za-z0-9]{4}[A-Za-z0-9]{4}[A-Za-z0-9]{4}[A-Za-z0-9]{12}";
            set_Session();
            get_ConfigData(configPath);

            ontologyAppDbConnector = new OntologyModDBConnector(Server,Port,Index,Index_Rep,SearchRange,Session);

            LogStates = new clsLogStates();
            DataTypes = new clsDataTypes();
            Classes = new clsClasses();
            ClassAtts = new clsBaseClassAttributes();
            ClassRels = new clsBaseClassRelation();
            RelationTypes = new clsRelationTypes();
            AttributeTypes = new clsAttributeTypes();
            Directions = new clsDirections();
            Variables = new clsVariables();
            MappingRules = new clsMappingRules();
            OntologyRelationRules = new clsOntologyRelationRules();


            var objOItem_Result = LogStates.LogState_Success;

           
            objOItem_Result = test_Existance_OntologyDB();

            try
            {
            


            if (objOItem_Result.GUID == LogStates.LogState_Nothing.GUID)
            {
                if (noService != null)
                {
                    noService();    
                }
                objOItem_Result = create_Index();
            }
                

            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {
                objOItem_Result = test_Existance_BaseData();
                if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
                {
                    objTransaction = new clsTransaction(this);

                    set_Computer();
                }
                else
                {
                    if (inconsistentData != null)
                    {
                        inconsistentData();   
                    }
                    
                }


                //if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
                //{
                //    if (ModuleLoad)
                //    {
                //        LoadModules();
                //    }

                //}

            }
            }
            catch (Exception ex)
            {
                if (checkConfig != null)
                {
                    checkConfig();
                }

            }
        }
        


    private clsOntologyItem test_Existance_BaseData()
    {
        
        var objOItem_Result = LogStates.LogState_Success;

        //DataTypes
        objOItem_Result = ontologyAppDbConnector.GetDataDataTypes(null, false);
        if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
        {
            var objOList_DType_NotExistant = (from objDataTypeShould in DataTypes.DataTypes
                                              join objDataTypeExist in ontologyAppDbConnector.DataTypes on objDataTypeShould.GUID equals objDataTypeExist.GUID into objDataTypesExist
                                              from objDataTypeExist in objDataTypesExist.DefaultIfEmpty()
                                              where objDataTypeExist == null
                               select objDataTypeShould).ToList();

            if (objOList_DType_NotExistant.Any())
            {
                objOItem_Result = ontologyAppDbConnector.SaveDataTypes(objOList_DType_NotExistant);
            }
        }

        //AttributeTypes
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            objOItem_Result = ontologyAppDbConnector.GetDataAttributeType(AttributeTypes.AttributeTypes);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {
                var objOList_AttTypes_NotExistant = (from objAttTypeShould in AttributeTypes.AttributeTypes
                                                     join objAttTypeExist in ontologyAppDbConnector.AttributeTypes on objAttTypeShould.GUID equals objAttTypeExist.GUID into objAttTypesExist
                                                     from objAttTypeExist in objAttTypesExist.DefaultIfEmpty()
                                                     where objAttTypeExist == null
                                                     select objAttTypeShould).ToList();

                if (objOList_AttTypes_NotExistant.Any())
                {
                    objOItem_Result = ontologyAppDbConnector.SaveAttributeTypes(objOList_AttTypes_NotExistant);
                }
            }
        }

        //RelationTypes
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            objOItem_Result = ontologyAppDbConnector.GetDataRelationTypes(RelationTypes.RelationTypes);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {
                var objOList_RelTypes_NotExistant = (from objRelTypeShould in RelationTypes.RelationTypes
                                                     join objRelTypeExist in ontologyAppDbConnector.RelationTypes on objRelTypeShould.GUID equals objRelTypeExist.GUID into objRelTypesExist 
                                                     from objRelTypeExist in objRelTypesExist.DefaultIfEmpty()
                                                     where objRelTypeExist == null
                                                     select objRelTypeShould).ToList();

                if (objOList_RelTypes_NotExistant.Any())
                {
                    ontologyAppDbConnector.SaveRelationTypes(objOList_RelTypes_NotExistant);
                }
            }
        }

        //Classes
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            objOItem_Result = ontologyAppDbConnector.GetDataClasses(Classes.OList_Classes);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {
                var objOList_Classes_NotExistant = (from objClassShould in Classes.OList_Classes
                                                    join objClassExist in ontologyAppDbConnector.Classes1 on objClassShould.GUID equals objClassExist.GUID into objClassesExist
                                                    from objClassExist in objClassesExist.DefaultIfEmpty()
                                                    where objClassExist == null
                                                    select objClassShould).ToList();
                if (objOList_Classes_NotExistant.Any())
                {
                    objOItem_Result = ontologyAppDbConnector.SaveClass(objOList_Classes_NotExistant);
                        

                }
            }
        }

        //Objects
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            var objOList_Objects = Directions.Directions.ToList();
            objOList_Objects.AddRange(LogStates.LogStates);
            objOList_Objects.AddRange(OntologyRelationRules.OntologyRelationRules);
            objOList_Objects.AddRange(Variables.Variables);
            objOList_Objects.AddRange(MappingRules.MappingRules);

            objOItem_Result = ontologyAppDbConnector.GetDataObjects(objOList_Objects);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {
                var objOList_Objects_NotExistant = (from objObjectShould in objOList_Objects
                                                    join objObjectExist in ontologyAppDbConnector.Objects1 on objObjectShould.GUID equals objObjectExist.GUID into objObjectsExists
                                                    from objObjectExist in objObjectsExists.DefaultIfEmpty()
                                                    where objObjectExist == null
                                                    select objObjectShould).ToList();
                if (objOList_Objects_NotExistant.Any())
                {
                    objOItem_Result = ontologyAppDbConnector.SaveObjects(objOList_Objects_NotExistant);

                }
            }
        }
        //ClassAttributes
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            var objOList_Classes = ClassAtts.ClassAtts.GroupBy(p => p.ID_Class).ToList().Join(Classes.OList_Classes, l => l.Key, r => r.GUID, (l, r) => r).ToList();

            var objOList_AttributeTypes = ClassAtts.ClassAtts.GroupBy(p => p.ID_AttributeType).ToList().Join(AttributeTypes.AttributeTypes, l => l.Key, r => r.GUID, (l, r) => r).ToList();

            objOItem_Result = ontologyAppDbConnector.GetDataClassAtts(objOList_Classes, objOList_AttributeTypes);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {

                var objOList_ClassAtts_NotExistant = (from objClassAttShould in ClassAtts.ClassAtts
                                                      join objClassAttExist in ontologyAppDbConnector.ClassAtts on 
                                            new { ID_Class = objClassAttShould.ID_Class, ID_AttributeType = objClassAttShould.ID_AttributeType} equals new { ID_Class = objClassAttExist.ID_Class, ID_AttributeType = objClassAttExist.ID_AttributeType} into objClassAttsExist
                                          from objClassAttExist in objClassAttsExist.DefaultIfEmpty()
                                          where objClassAttExist == null
                                          select objClassAttShould).ToList();

                
                if (objOList_ClassAtts_NotExistant.Any())
                {
                    objOItem_Result = ontologyAppDbConnector.SaveClassAtt(objOList_ClassAtts_NotExistant);
                }
            }

            


        }
        //ClassRelations
        if (objOItem_Result.GUID != LogStates.LogState_Error.GUID)
        {
            objOItem_Result = ontologyAppDbConnector.GetDataClassRel(ClassRels.ClassRelations);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {

                var objOList_ClassRels_NotExistant = (from objClassRelShould in ClassRels.ClassRelations
                                                      join objClassRelExist in ontologyAppDbConnector.ClassRels on
                                          new { ID_Class_Left = objClassRelShould.ID_Class_Left, 
                                                ID_Class_Right = objClassRelShould.ID_Class_Right, 
                                                ID_RelationType = objClassRelShould.ID_RelationType } equals 
                                          new { ID_Class_Left = objClassRelExist.ID_Class_Left, 
                                                ID_Class_Right = objClassRelExist.ID_Class_Right,
                                                ID_RelationType = objClassRelExist.ID_RelationType } into objClassRelsExist 
                                          from objClassRelExist in objClassRelsExist.DefaultIfEmpty()
                                          where objClassRelExist == null
                                          select objClassRelShould).ToList();

         
                if (objOList_ClassRels_NotExistant.Any())
                {
                    objOItem_Result = ontologyAppDbConnector.SaveClassRel(ClassRels.ClassRelations);
                }

            }
        }
        return objOItem_Result;

    }

    private clsOntologyItem test_Existance_OntologyDB()
    {
        if (ontologyAppDbConnector.TestIndexEs())
        {
            return LogStates.LogState_Success;
        }
        else
        {
            return LogStates.LogState_Nothing;
        }
    }

        public clsOntologyItem create_Index()
        {
            return ontologyAppDbConnector.CreateIndex();
        }
        
        private void set_Session()
        {
            GUID_Session = Guid.NewGuid().ToString().Replace("-", "");
        }
    
        private void set_Computer()
        {
            ManagementObjectSearcher objWMI;
            string strProcessorID;
            string strBaseBoardSerial;
            clsOntologyItem objOItem_Result;

            clsObjectAtt objOAItem_ProcessorID;
            clsObjectAtt objOAItem_BaseBoardSerial;

            strProcessorID = "";
            objWMI = new ManagementObjectSearcher("Select ProcessorID FROM Win32_Processor");
            foreach (ManagementObject objWMIManagementObject in objWMI.Get())
            {
                strProcessorID = objWMIManagementObject["ProcessorID"].ToString();
                break;
            }

            strBaseBoardSerial = "";
            objWMI = new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_Baseboard");
            foreach (ManagementObject objWMIManagementObject in objWMI.Get())
            {
                strBaseBoardSerial = objWMIManagementObject["SerialNumber"].ToString();
                break;
            }

            objOItem_Server = get_Computer_ByWMI(strProcessorID, strBaseBoardSerial);

            if (objOItem_Server == null)
            {
                objOItem_Server = new clsOntologyItem();
                objOItem_Server.GUID = Guid.NewGuid().ToString();
                objOItem_Server.Name = Environment.MachineName;
                objOItem_Server.GUID_Parent = Classes.OItem_Class_Server.GUID;
                objOItem_Server.Type = Type_Object;



                objOItem_Result = objTransaction.do_Transaction(objOItem_Server);

                if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
                {
                    objOAItem_BaseBoardSerial = new clsObjectAtt() {ID_AttributeType = AttributeTypes.OITem_AttributeType_WMI_BaseBoardSerial.GUID,
                                                                    ID_Object = objOItem_Server.GUID,
                                                                    ID_Class = objOItem_Server.GUID_Parent,
                                                                    ID_DataType = DataTypes.DType_String.GUID, 
                                                                    OrderID = 1,
                                                                    Val_String = strBaseBoardSerial,
                                                                    Val_Named = strBaseBoardSerial};

                    objOItem_Result = objTransaction.do_Transaction(objOAItem_BaseBoardSerial);

                    if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
                    {
                        objOAItem_ProcessorID = new clsObjectAtt() {ID_AttributeType = AttributeTypes.OItem_AttributeType_WMI_ProcessorID.GUID, 
                                                                           ID_Object = objOItem_Server.GUID,
                                                                           ID_Class = objOItem_Server.GUID_Parent,
                                                                           ID_DataType = DataTypes.DType_String.GUID,
                                                                           OrderID = 1,
                                                                           Val_String = strProcessorID,
                                                                           Val_Named = strProcessorID};

                        objOItem_Result = objTransaction.do_Transaction(objOAItem_ProcessorID);
                        if (objOItem_Result.GUID == LogStates.LogState_Error.GUID)
                        {
                            objTransaction.rollback();
                            throw new Exception("Config");
                        }
                    }
                    else
                    {
                        objTransaction.rollback();
                        throw new Exception("Config");
                    }
                }
                else
                {
                    throw new Exception("Config");
                }
            }
        }

        public clsOntologyItem get_Computer_ByWMI(string strProcessorID, string strBaseBoardSerial)
        {
            var objOAL_WMIData = new List<clsObjectAtt>();
            var objOAL_BaseBoardSerial = new List<clsObjectAtt>();
            clsOntologyItem objOItem_Result;
            clsOntologyItem objOItem_Computer = null;

            objOAL_WMIData.Add(new clsObjectAtt
            {
                ID_Class = Classes.OItem_Class_Server.GUID,
                ID_AttributeType = AttributeTypes.OItem_AttributeType_WMI_ProcessorID.GUID
            });

            objOAL_WMIData.Add(new clsObjectAtt
            {
                ID_Class = Classes.OItem_Class_Server.GUID,
                ID_AttributeType = AttributeTypes.OITem_AttributeType_WMI_BaseBoardSerial.GUID
            });


            objOItem_Result = ontologyAppDbConnector.GetDataObjectAtt(objOAL_WMIData);
            if (objOItem_Result.GUID == LogStates.LogState_Success.GUID)
            {


                var objOList_Server = (from objServer in ontologyAppDbConnector.ObjAtts.Where(proc => proc.ID_AttributeType == AttributeTypes.OItem_AttributeType_WMI_ProcessorID.GUID && proc.Val_String == strProcessorID)
                                       join objServer2 in ontologyAppDbConnector.ObjAtts.Where(proc => proc.ID_AttributeType == AttributeTypes.OITem_AttributeType_WMI_BaseBoardSerial.GUID && proc.Val_String == strBaseBoardSerial) on objServer.ID_Object equals objServer2.ID_Object
                                       select new clsOntologyItem { GUID = objServer.ID_Object, Name = objServer.Name_Object, GUID_Parent = objServer.ID_Class, Type = Type_Object }).ToList();

                if (objOList_Server.Any())
                {
                    objOItem_Computer = objOList_Server.First();
                }
                else
                {
                    objOItem_Computer = null;
                }
            }
            else
            {
                throw new Exception("Config");
            }

            return objOItem_Computer;
        }
    
        public string GetConfigName1(string strName)
        {
            string strResult = "";
            for (var i = 0;i<= strName.Length - 1;i++)
            {
                if (Char.IsLetterOrDigit(strName,i))
                {
                    strResult = strResult + strName.Substring(i, 1);
                }
                else
                {
                    strResult = strResult + "_";
                }
                
                    
            }

            return strResult;
        }


        public string CalculateMD5HashAsGuid(string input)

        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {

                sb.Append(hash[i].ToString("X2"));

            }
            sb.Append("AAA");

            var result = $"{sb.ToString().Substring(0, 8)}-{sb.ToString().Substring(8, 4)}-{sb.ToString().Substring(12, 4)}-{sb.ToString().Substring(16, 4)}-{sb.ToString().Substring(20, 12)}";
            return result;

        }
    }
}
