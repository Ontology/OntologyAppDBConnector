﻿using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.ModelWriters
{
    public class ModelItem
    {
        public object Item { get; set; }
        public string IdParent { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public clsOntologyItem ObjectItem { get; set; }

        public ModelItem(object item, string idParent, string type)
        {
            Item = item;
            IdParent = idParent;
            GetObjectItem(type);
        }

        private void GetObjectItem(string type)
        {
            var properties = Item.GetType().GetProperties();

            foreach(PropertyInfo prop in properties)
            {
                OntologyObjectAttribute attributeItem = (OntologyObjectAttribute) prop.GetCustomAttribute(typeof( OntologyObjectAttribute));

                if (attributeItem != null)
                {
                    if (attributeItem.PropertyType == PropertyType.Guid)
                    {
                        var id = prop.GetValue(Item).ToString();
                        Guid guidCheck;
                        if (Guid.TryParse(id, out guidCheck))
                        {
                            Id = guidCheck.ToString().Replace("-", "");
                        }
                        else
                        {
                            Id = null;
                        }
                    }
                    else if (attributeItem.PropertyType == PropertyType.Name)
                    {
                        var name = prop.GetValue(Item).ToString();

                        if (name.Length>255)
                        {
                            name = name.Substring(1, 254);
                        }

                        Name = name;
                    }
                }
            }

            if (!string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(IdParent))
            {
                ObjectItem = new clsOntologyItem
                {
                    GUID = Id,
                    Name = Name,
                    GUID_Parent = IdParent,
                    Type = type
                };
            }
        }
    }
}
