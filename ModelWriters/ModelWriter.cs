﻿using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.ModelWriters
{
    public class ModelWriter
    {
        private OntologyModDBConnector dbWriter;
        private OntologyModDBConnector dbReader;

        private clsLogStates logStates = new clsLogStates();
        private clsTypes types = new clsTypes();

        private List<clsOntologyItem> ObjectsToSave;

        public List<object> GetSubObjects(object item)
        {
            var properties = item.GetType().GetProperties().ToList();

            var result = new List<object>();

            foreach (PropertyInfo prop in properties)
            {
                var propItem = prop.GetValue(item);
                if (propItem != null)
                {
                    if (IsGenericList(propItem))
                    {
                        var type = propItem.GetType();
                        IEnumerable enumerable = propItem as IEnumerable;

                        foreach (var propItm in enumerable)
                        {
                            var classAttribute = propItm.GetType().GetCustomAttributes<OntologyClassAttribute>(true).FirstOrDefault();

                            if (classAttribute != null)
                            {
                                result.Add(propItm);
                            }
                            result.AddRange(GetSubObjects(propItm));
                        }

                    }
                    else
                    {
                        if (!propItem.GetType().IsPrimitive && !(propItem.GetType() == typeof(string)))
                        {
                            var classAttribute = propItem.GetType().GetCustomAttributes<OntologyClassAttribute>(true).FirstOrDefault();

                            if (classAttribute != null)
                            {
                                result.Add(propItem);
                            }
                        }

                    }
                }
            }

            return result;
        }

        public clsOntologyItem SaveModel(List<object> itemsToSave)
        {
            var result = logStates.LogState_Error.Clone();

            var modelItems = new List<ModelItem>();
            ObjectsToSave = new List<clsOntologyItem>();

            var items = new List<object>();
            foreach (var item in itemsToSave)
            {
                items.AddRange(GetSubObjects(item));
            }
            itemsToSave.AddRange(items);
            var typeList = itemsToSave.GroupBy(item => item.GetType());

            foreach (var typeItem in typeList)
            {

                var classAttribute = typeItem.Key.GetCustomAttributes(true).Cast<OntologyClassAttribute>().Where(itemType => itemType != null).FirstOrDefault();

                if (classAttribute != null)
                {
                    var idParent = classAttribute.IdClass;
                    Guid guidCheck;
                    if (Guid.TryParse(idParent, out guidCheck))
                    {
                        var newModelItems = itemsToSave.Where(item => item.GetType() == typeItem.Key).Select(modelItem => new ModelItem(modelItem, idParent, types.ObjectType)).ToList();



                        var oItems = newModelItems.Where(modelItem => modelItem.ObjectItem != null).Select(modelItem => modelItem.ObjectItem).ToList();



                        ObjectsToSave.AddRange(oItems);
                        newModelItems = newModelItems.Where(modelItem => modelItem.ObjectItem != null).ToList();
                        modelItems.AddRange(newModelItems);
                    }



                }
            }

            if (ObjectsToSave.Any())
            {
                result = dbWriter.SaveObjects(ObjectsToSave);
            }
            else
            {
                result = logStates.LogState_Nothing.Clone();
            }

            if (result.GUID == logStates.LogState_Success.GUID)
            {
                List<ModelRelationItem> modelRelationItems = new List<ModelRelationItem>();
                List<ModelAttributeItem> modelAttributeItems = new List<ModelAttributeItem>();
                modelItems.ForEach(item =>
                {
                    var propertiesObjectAttributes = item.Item.GetType().GetProperties().Where(prop => prop.GetCustomAttributes(true).Where(attrib => attrib.GetType() == typeof(ObjectAttributeAttribute)).Cast<ObjectAttributeAttribute>().FirstOrDefault() != null).ToList();
                    var propertiesObjectRelations = item.Item.GetType().GetProperties().Where(prop => prop.GetCustomAttributes(true).Where(attrib => attrib.GetType() == typeof(ObjectRelationAttribute)).Cast<ObjectRelationAttribute>().FirstOrDefault() != null).ToList();

                    propertiesObjectAttributes.ForEach(prop =>
                    {
                        var attributeItem = prop.GetCustomAttributes(true).Where(attrib => attrib.GetType() == typeof(ObjectAttributeAttribute)).Cast<ObjectAttributeAttribute>().FirstOrDefault();
                        if (attributeItem != null && attributeItem.IdAttributeType != null)
                        {
                            var modelAttributeItem = new ModelAttributeItem();
                            modelAttributeItem.LeftItem = item;
                            modelAttributeItem.Replace = attributeItem.Replace;
                            modelAttributeItem.IdAttributeType = attributeItem.IdAttributeType;
                            modelAttributeItem.ValueItem = prop.GetValue(item.Item);
                            modelAttributeItems.Add(modelAttributeItem);
                        }
                    });

                    propertiesObjectRelations.ForEach(prop =>
                    {
                        var attributeItem = prop.GetCustomAttributes(true).Where(attrib => attrib.GetType() == typeof(ObjectRelationAttribute)).Cast<ObjectRelationAttribute>().FirstOrDefault();
                        if (attributeItem != null)
                        {
                            if (attributeItem.RelationSearchType == RelationSearchType.Name)
                            {

                                var nameObjectFull = prop.GetValue(item.Item).ToString();
                                var nameObject = nameObjectFull;

                                if (nameObject.Length > 255)
                                {
                                    nameObject = nameObject.Substring(0, 254);
                                }

                                if (!string.IsNullOrEmpty(nameObject) && !string.IsNullOrEmpty(attributeItem.IdParentOther) && !string.IsNullOrEmpty(attributeItem.IdRelationType))
                                {
                                    modelRelationItems.Add(new ModelRelationItem
                                    {
                                        LeftItem = item,
                                        PropertyItem = prop,
                                        RelationAttribute = attributeItem,
                                        NameItem = nameObject,
                                        SearchItem = new clsOntologyItem
                                        {
                                            Name = nameObject,
                                            GUID_Parent = attributeItem.IdParentOther
                                        },
                                        IdRelationType = attributeItem.IdRelationType
                                    });

                                }
                            }
                            else if (attributeItem.RelationSearchType == RelationSearchType.OntologyItem)
                            {
                                var subItem = prop.GetValue(item.Item);
                                var idObject = item.Id;
                                var idParentObject = item.IdParent;
                                var idRelationType = attributeItem.IdRelationType;
                                if (subItem != null && idRelationType != null)
                                {

                                    if (IsGenericList(subItem))
                                    {
                                        List<clsObjectRel> objectRelations = new List<clsObjectRel>();
                                        var objectList = ((IEnumerable<object>)subItem).Cast<object>().ToList();
                                        objectList.ForEach(objList =>
                                        {
                                            var propertyItemForGuid = objList.GetType().GetProperties().
                                                Cast<PropertyInfo>().Where(propItem => propItem.GetCustomAttribute(typeof(OntologyObjectAttribute)) != null).
                                                Where(propItem =>
                                                    ((OntologyObjectAttribute)propItem.GetCustomAttribute(typeof(OntologyObjectAttribute))).PropertyType == PropertyType.Guid).FirstOrDefault();

                                            if (propertyItemForGuid != null)
                                            {
                                                var guid = propertyItemForGuid.GetValue(objList).ToString();
                                                Guid guidItem;
                                                if (guid != null && Guid.TryParse(guid, out guidItem))
                                                {

                                                    var rightItem = modelItems.Where(modelItem => modelItem.ObjectItem.GUID.ToLower() == guid.ToLower().Replace("-", "")).FirstOrDefault();

                                                    if (rightItem != null)
                                                    {
                                                        objectRelations.Add(new clsObjectRel
                                                        {
                                                            ID_Object = idObject,
                                                            ID_Parent_Object = idParentObject,
                                                            ID_RelationType = idRelationType,
                                                            ID_Other = rightItem.ObjectItem.GUID,
                                                            ID_Parent_Other = rightItem.ObjectItem.GUID_Parent,
                                                            Ontology = rightItem.ObjectItem.Type,
                                                            OrderID = 1
                                                        });
                                                            //var objectRelations = modelRelationItems.Where(modRelItem => modRelItem.LeftItem != null && modRelItem.RightItem != null && modRelItem.IdRelationType != null).Select(modRelItem => new clsObjectRel
                                                            //{
                                                            //    ID_Object = modRelItem.LeftItem.ObjectItem.GUID,
                                                            //    ID_Parent_Object = modRelItem.LeftItem.ObjectItem.GUID_Parent,
                                                            //    ID_Other = modRelItem.RightItem.GUID,
                                                            //    ID_Parent_Other = modRelItem.RightItem.GUID_Parent,
                                                            //    ID_RelationType = modRelItem.IdRelationType,
                                                            //    Ontology = modRelItem.RightItem.Type,
                                                            //    OrderID = 1
                                                            //}).ToList();
                                                        }
                                                }
                                            }


                                        });

                                        if (objectRelations.Any())
                                        {
                                            result = dbWriter.SaveObjRel(objectRelations);
                                        }
                                    }
                                    else
                                    {
                                        List<clsObjectRel> objectRelations = new List<clsObjectRel>();
                                        var propertyItemForGuid = subItem.GetType().GetProperties().
                                                Cast<PropertyInfo>().Where(propItem => propItem.GetCustomAttribute(typeof(OntologyObjectAttribute)) != null).
                                                Where(propItem =>
                                                    ((OntologyObjectAttribute)propItem.GetCustomAttribute(typeof(OntologyObjectAttribute))).PropertyType == PropertyType.Guid).FirstOrDefault();

                                        if (propertyItemForGuid != null)
                                        {
                                            var guid = propertyItemForGuid.GetValue(subItem).ToString();
                                            Guid guidItem;
                                            if (guid != null && Guid.TryParse(guid, out guidItem))
                                            {

                                                var rightItem = modelItems.Where(modelItem => modelItem.ObjectItem.GUID == guid.Replace("-", "")).FirstOrDefault();

                                                if (rightItem != null)
                                                {
                                                    objectRelations.Add(new clsObjectRel
                                                    {
                                                        ID_Object = idObject,
                                                        ID_Parent_Object = idParentObject,
                                                        ID_RelationType = idRelationType,
                                                        ID_Other = rightItem.ObjectItem.GUID,
                                                        ID_Parent_Other = rightItem.ObjectItem.GUID_Parent,
                                                        Ontology = rightItem.ObjectItem.Type,
                                                        OrderID = 1
                                                    });
                                                    //var objectRelations = modelRelationItems.Where(modRelItem => modRelItem.LeftItem != null && modRelItem.RightItem != null && modRelItem.IdRelationType != null).Select(modRelItem => new clsObjectRel
                                                    //{
                                                    //    ID_Object = modRelItem.LeftItem.ObjectItem.GUID,
                                                    //    ID_Parent_Object = modRelItem.LeftItem.ObjectItem.GUID_Parent,
                                                    //    ID_Other = modRelItem.RightItem.GUID,
                                                    //    ID_Parent_Other = modRelItem.RightItem.GUID_Parent,
                                                    //    ID_RelationType = modRelItem.IdRelationType,
                                                    //    Ontology = modRelItem.RightItem.Type,
                                                    //    OrderID = 1
                                                    //}).ToList();
                                                }
                                                else
                                                {
                                                    var oItem = dbReader.GetOItem(guid.Replace("-", ""), types.ObjectType);
                                                    if (oItem != null)
                                                    {
                                                        objectRelations.Add(new clsObjectRel
                                                        {
                                                            ID_Object = idObject,
                                                            ID_Parent_Object = idParentObject,
                                                            ID_RelationType = idRelationType,
                                                            ID_Other = oItem.GUID,
                                                            ID_Parent_Other = oItem.GUID_Parent,
                                                            Ontology = oItem.Type,
                                                            OrderID = 1
                                                        });
                                                    }
                                                }
                                            }

                                        }

                                        if (objectRelations.Any())
                                        {
                                            result = dbWriter.SaveObjRel(objectRelations);
                                        }
                                    }
                                }

                            }
                        }
                    });
                });

                if (modelRelationItems.Any())
                {
                    result = dbReader.GetDataObjects(modelRelationItems.Select(modelRel => modelRel.SearchItem).ToList());

                    if (result.GUID == logStates.LogState_Success.GUID)
                    {
                        modelRelationItems.ForEach(modelItem =>
                        {
                            var foundItem = dbReader.Objects1.Where(obj => obj.Name == modelItem.NameItem).FirstOrDefault();

                            if (foundItem != null)
                            {
                                modelItem.RightItem = foundItem;
                            }
                            else
                            {
                                modelItem.RightItem = new clsOntologyItem
                                {
                                    GUID = Guid.NewGuid().ToString().Replace("-", ""),
                                    Name = modelItem.NameItem,
                                    GUID_Parent = modelItem.SearchItem.GUID_Parent,
                                    Type = types.ObjectType,
                                    Mark = true
                                };
                            }
                        });

                        var newRightItem = modelRelationItems.Where(modelItem => modelItem.RightItem.Mark == true).Select(modelItem => modelItem.RightItem).ToList();

                        if (newRightItem.Any())
                        {
                            result = dbWriter.SaveObjects(newRightItem);
                        }


                        if (result.GUID == logStates.LogState_Success.GUID)
                        {
                            var objectRelations = modelRelationItems.Where(modRelItem => modRelItem.LeftItem != null && modRelItem.RightItem != null && modRelItem.IdRelationType != null).Select(modRelItem => new clsObjectRel
                            {
                                ID_Object = modRelItem.LeftItem.ObjectItem.GUID,
                                ID_Parent_Object = modRelItem.LeftItem.ObjectItem.GUID_Parent,
                                ID_Other = modRelItem.RightItem.GUID,
                                ID_Parent_Other = modRelItem.RightItem.GUID_Parent,
                                ID_RelationType = modRelItem.IdRelationType,
                                Ontology = modRelItem.RightItem.Type,
                                OrderID = 1
                            }).ToList();

                            if (objectRelations.Any())
                            {
                                result = dbWriter.SaveObjRel(objectRelations);
                            }
                        }


                    }
                }

                if (result.GUID == logStates.LogState_Success.GUID)
                {
                    var modelAttributeItemsToDelete = modelAttributeItems.Where(modelAttirbute => modelAttirbute.Replace && modelAttirbute.ObjectAttributeDel != null).Select(modelAttribute => modelAttribute.ObjectAttributeDel).ToList();

                    result = dbWriter.GetDataObjectAtt(modelAttributeItemsToDelete);
                    
                    if (result.GUID == logStates.LogState_Success.GUID)
                    {
                        modelAttributeItemsToDelete = dbWriter.ObjAtts;
                        if (modelAttributeItemsToDelete.Any())
                        {
                            result = dbWriter.DelObjectAtts(modelAttributeItemsToDelete);
                        }
                    }
                    



                }

                if (result.GUID == logStates.LogState_Success.GUID || result.GUID == logStates.LogState_Nothing.GUID)
                {
                    var modelAttributeItemsToSave = modelAttributeItems.Where(modeLAttribute => modeLAttribute.ObjectAttribute != null).ToList();
                    if (modelAttributeItemsToSave.Any())
                    {
                        result = dbWriter.SaveObjAtt(modelAttributeItemsToSave.Select(modelAttribut => modelAttribut.ObjectAttribute).ToList());
                    }
                }

            }




            return result;
        }

        public bool IsGenericList(object o)
        {
            bool isGenericList = false;

            var oType = o.GetType();

            if (oType.IsGenericType && (oType.GetGenericTypeDefinition() == typeof(List<>)))
                isGenericList = true;

            return isGenericList;
        }

        public ModelWriter(OntologyModDBConnector dbWriter)
        {
            this.dbWriter = dbWriter;
            dbReader = new OntologyModDBConnector(dbWriter.Server, dbWriter.Port, dbWriter.Index, dbWriter.IndexRep, dbWriter.SearchRange, dbWriter.Session);
        }
    }
}
