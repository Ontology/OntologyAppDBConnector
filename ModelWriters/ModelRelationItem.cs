﻿using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.ModelWriters
{
    public class ModelRelationItem
    {
        public ModelItem LeftItem { get; set; }
        public clsOntologyItem RightItem { get; set; }
        public string NameItem { get; set; }
        public PropertyInfo PropertyItem { get; set; }
        public ObjectRelationAttribute RelationAttribute { get; set; }
        public clsOntologyItem SearchItem { get; set; }
        public string IdRelationType { get; set; }
    }
}
