﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector.ModelWriters
{
    class ModelAttributeItem
    {
        private clsDataTypes dataTypes = new clsDataTypes();
        public ModelItem LeftItem { get; set; }
        public bool Replace { get; set; }
        public string IdAttributeType { get; set; }
        private object valueItem;
        public object ValueItem
        {
            get { return valueItem; }
            set
            {
                valueItem = value;
                ObjectAttribute = null;
                ObjectAttributeDel = null;
                long longValue;
                ObjectAttributeDel = new clsObjectAtt
                {
                    ID_AttributeType = IdAttributeType,
                    ID_Object = LeftItem.ObjectItem.GUID
                };
                if (valueItem != null)
                {
                    if (valueItem is string)
                    {
                        ObjectAttribute = new clsObjectAtt
                        {
                            Val_String = valueItem.ToString(),
                            Val_Name = valueItem.ToString().Length > 255 ? valueItem.ToString().Substring(0, 254) : valueItem.ToString(),
                            ID_DataType = dataTypes.DType_String.GUID,
                            ID_AttributeType = IdAttributeType,
                            ID_Attribute = Guid.NewGuid().ToString().Replace("-", ""),
                            OrderID = 1,
                            ID_Object = LeftItem.ObjectItem.GUID,
                            ID_Class = LeftItem.ObjectItem.GUID_Parent
                        };
                    }
                    else if (valueItem is DateTime)
                    {
                        ObjectAttribute = new clsObjectAtt
                        {
                            Val_Date = (DateTime)valueItem,
                            Val_Name = valueItem.ToString(),
                            ID_DataType = dataTypes.DType_DateTime.GUID,
                            ID_AttributeType = IdAttributeType,
                            ID_Attribute = Guid.NewGuid().ToString().Replace("-", ""),
                            OrderID = 1,
                            ID_Object = LeftItem.ObjectItem.GUID,
                            ID_Class = LeftItem.ObjectItem.GUID_Parent
                        };
                    }
                    else if (valueItem is bool)
                    {
                        ObjectAttribute = new clsObjectAtt
                        {
                            Val_Bool = (bool)valueItem,
                            Val_Name = valueItem.ToString(),
                            ID_DataType = dataTypes.DType_Bool.GUID,
                            ID_AttributeType = IdAttributeType,
                            ID_Attribute = Guid.NewGuid().ToString().Replace("-", ""),
                            OrderID = 1,
                            ID_Object = LeftItem.ObjectItem.GUID,
                            ID_Class = LeftItem.ObjectItem.GUID_Parent
                        };
                    }
                    else if (valueItem is double)
                    {
                        ObjectAttribute = new clsObjectAtt
                        {
                            Val_Double = (double)valueItem,
                            Val_Name = valueItem.ToString(),
                            ID_DataType = dataTypes.DType_Real.GUID,
                            ID_AttributeType = IdAttributeType,
                            ID_Attribute = Guid.NewGuid().ToString().Replace("-", ""),
                            OrderID = 1,
                            ID_Object = LeftItem.ObjectItem.GUID,
                            ID_Class = LeftItem.ObjectItem.GUID_Parent
                        };
                    }
                    else if (long.TryParse(valueItem.ToString(), out longValue))
                    {
                        ObjectAttribute = new clsObjectAtt
                        {
                            Val_Lng = longValue,
                            Val_Name = longValue.ToString(),
                            ID_DataType = dataTypes.DType_Int.GUID,
                            ID_AttributeType = IdAttributeType,
                            ID_Attribute = Guid.NewGuid().ToString().Replace("-", ""),
                            OrderID = 1,
                            ID_Object = LeftItem.ObjectItem.GUID,
                            ID_Class = LeftItem.ObjectItem.GUID_Parent
                        };
                    }
                }
                


            }
        }

        public clsObjectAtt ObjectAttribute
        {
            get; set;
        }
        public clsObjectAtt ObjectAttributeDel
        {
            get; set;
        }
    }
}
