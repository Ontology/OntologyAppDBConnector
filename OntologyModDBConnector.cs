﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using ElasticSearchNestConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyAppDBConnector.Attributes;
using OntologyAppDBConnector.ModelWriters;
using Nest;

namespace OntologyAppDBConnector
{
    public class OntologyModDBConnector: IDisposable
    {
        public SortEnum ASC_Name { get { return SortEnum.ASC_Name;} }
        public SortEnum DESC_Name { get { return SortEnum.DESC_Name;} }
        public SortEnum ASC_OrderId { get { return SortEnum.DESC_OrderID; } }
        public SortEnum DESC_OrderId { get { return SortEnum.DESC_OrderID;} }
        public SortEnum None { get { return SortEnum.NONE; } }

        private List<clsOntologyItem> objects1;
 
        public List<clsOntologyItem> Objects1 
        {
            get {
                if (Sort == SortEnum.ASC_Name)
                {
                    return objects1.OrderBy(obj => obj.Name).ToList();
                }
                else if (Sort == SortEnum.DESC_Name)
                {
                    return objects1.OrderByDescending(obj => obj.Name).ToList();
                }
                else
                {
                    return objects1;
                }
            }
            set { objects1 = value; }
        }
        public List<clsOntologyItem> Objects2 { get; set; }
        private List<clsObjectRel> objectRelsId;

        public List<clsObjectRel> ObjectRelsId
        {
            get
            {
                if (Sort == SortEnum.ASC_OrderID)
                {
                    return objectRelsId.OrderBy(objRel => objRel.OrderID).ToList();
                }
                else if (Sort == SortEnum.DESC_OrderID)
                {
                    return objectRelsId.OrderByDescending(objRel => objRel.OrderID).ToList();
                }
                else
                {
                    return objectRelsId;
                }
                
            }
            set { objectRelsId = value; }
        }

        private List<clsObjectRel> objectRels;

        public List<clsObjectRel> ObjectRels
        {
            get
            {
                if (Sort == SortEnum.ASC_OrderID)
                {
                    return objectRels.OrderBy(objRel => objRel.OrderID).ToList();
                }
                else if (Sort == SortEnum.DESC_OrderID)
                {
                    return objectRels.OrderByDescending(objRel => objRel.OrderID).ToList();
                }
                else if (Sort == SortEnum.ASC_Name)
                {
                    return objectRels.OrderBy(objRel => objRel.Name_Other).ToList();
                }
                else if (Sort == SortEnum.DESC_Name)
                {
                    return objectRels.OrderByDescending(objRel => objRel.Name_Other).ToList();
                }
                else
                {
                    return objectRels;
                }
            }
            set { objectRels = value; }
        }
        public List<clsObjectTree> ObjectTree { get; set; }
        public List<clsOntologyItem> Classes1 { get; set; }
        public List<clsOntologyItem> Classes2 { get; set; }
        public List<clsOntologyItem> RelationTypes { get; set; }
        public List<clsOntologyItem> AttributeTypes { get; set; }
        public List<clsClassRel> ClassRelsId { get; set; }
        public List<clsClassRel> ClassRels { get; set; }
        public List<clsClassAtt> ClassAttsId { get; set; }
        public List<clsClassAtt> ClassAtts { get; set; }
        public List<clsObjectAtt> ObjAttsId { get; set; }
        public List<clsObjectAtt> ObjAtts { get; set; }
        public List<clsOntologyItem> DataTypes { get; set; }
        public List<clsAttribute> Attributes { get; set; }

        private clsDataTypes dataTypes = new clsDataTypes();
        private clsTypes types = new clsTypes();
        private clsLogStates logStates = new clsLogStates();
        private clsFields fields = new clsFields();
        private clsDirections directions = new clsDirections();
        private clsClasses objClasses = new clsClasses();
        private clsRelationTypes relationTypes = new clsRelationTypes();
        private clsAttributeTypes attributeTypes = new clsAttributeTypes();

        private string server;
        public string Server
        {
            get
            {
                return server;
            }
        }

        private string index;
        public string Index
        {
            get
            {
                return index;
            }
        }

        private string indexRep;
        public string IndexRep
        {
            get
            {
                return indexRep;
            }
        }
        private int port;
        public int Port
        {
            get
            {
                return port;
            }
        }

        private int searchRange;
        public int SearchRange
        {
            get
            {
                return searchRange;
            }
        }
        private string session;
        public string Session
        {
            get
            {
                return session;
            }
        }

        public int PackageLength { get; set; }
        private SortEnum sortE;

        private clsDBSelector elSelector;
        private clsDBDeletor elDeletor;
        private clsDBUpdater elUpdater;

        public List<clsObjectAtt> SavedObjectAtts { get; private set; }

        public delegate void NamingError();

        public event NamingError _namingError;

        public SortEnum Sort
        {
            get { return sortE;}
            set
            {
                sortE = value;
                elSelector.Sort = Sort;
            }
        }
        
        public List<string> IndexList(string server, int port)
        {
            
            return elSelector.IndexList(server, port);
            
        }

        public clsOntologyItem DelAttributeTypes(List<clsOntologyItem> attributeTypes)
        {
            try
            {
                var result = elDeletor.del_AttributeType(attributeTypes);
                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
            
        }

        public clsOntologyItem DelRelationTypes(List<clsOntologyItem> relationTypes)
        {
            try
            {
                return elDeletor.del_RelationType(relationTypes);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelDataTypes(List<clsOntologyItem> dataTypes)
        {
            try
            {
                return elDeletor.del_DataType(dataTypes);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelClassAttType(clsOntologyItem classItem, clsOntologyItem attType)
        {
            try
            {
                return elDeletor.del_ClassAttType(classItem, attType);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelObjects(List<clsOntologyItem> objects)
        {
            try
            {
                return elDeletor.del_Objects(objects);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelClassRel(List<clsClassRel> classRels)
        {
            try
            {
                return elDeletor.del_ClassRel(classRels);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelObjectAtts(List<clsObjectAtt> objectAtts)
        {
            try
            {
                return elDeletor.del_ObjectAtt(objectAtts);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelObjectRels(List<clsObjectRel> objectRels)
        {
            try
            {
                return elDeletor.del_ObjectRel(objectRels);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem DelClasses(List<clsOntologyItem> classes)
        {
            try
            {
                return elDeletor.del_Class(classes);
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }



        public clsOntologyItem SaveDataTypes(List<clsOntologyItem> dataTypes)
        {
            var result = elUpdater.save_DataTypes(dataTypes);
            return result;
        }

        public clsOntologyItem SaveAttributeTypes(List<clsOntologyItem> attributeTypes)
        {
            
            foreach (var attributeType in attributeTypes)
            {
                var result = elUpdater.save_AttributeType(attributeType);
                if (result.GUID == logStates.LogState_Error.GUID)
                {
                    return logStates.LogState_Error.Clone();
                }
            }

            return logStates.LogState_Success.Clone();
        }


        public clsOntologyItem SaveClassRel(List<clsClassRel> classRels)
        {
            return elUpdater.save_ClassRel(classRels);
        }

        public clsOntologyItem SaveClassAtt(List<clsClassAtt> classAtts)
        {
            return elUpdater.save_ClassAtt(classAtts);
        }

        public clsOntologyItem SaveClass(List<clsOntologyItem> classes)
        {
            foreach (var classItem in classes)
            {
                var result = elUpdater.save_Class(classItem, string.IsNullOrEmpty(classItem.GUID_Parent));
                if (result.GUID == logStates.LogState_Error.GUID)
                {
                    return result;
                }
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SaveObjRel(List<clsObjectRel> objectRels)
        {
            return elUpdater.save_ObjectRel(objectRels);
        }

        public clsOntologyItem SaveObjAtt(List<clsObjectAtt> objectAtts)
        {
            var result = elUpdater.save_ObjectAtt(objectAtts);
            SavedObjectAtts = result.ObjectAttributes;
            return result.Result;
            
        }

        public clsOntologyItem SaveRelationTypes(List<clsOntologyItem> relationTypes)
        {
            var result = logStates.LogState_Success.Clone();
            foreach (var relationType in relationTypes)
            {
                result = elUpdater.save_RelationType(relationType);
                if (result.GUID == logStates.LogState_Error.GUID)
                {
                    return result;
                }
            }

            return result;
        }

        public clsOntologyItem SaveObjects(List<clsOntologyItem> objects)
        {
            var searchRules = objects.GroupBy(obj => obj.GUID_Parent).Select(parentGuid => new clsObjectRel
            {
                ID_Other = parentGuid.Key,
                ID_RelationType = relationTypes.OItem_RelationType_belongingClass.GUID,
                ID_Parent_Object = objClasses.OItem_Class_Ontology_Naming_Rule.GUID
            }).ToList();

            var namingRules = elSelector.get_Data_ObjectRel(searchRules, boolIDs: true);

            var result = logStates.LogState_Error.Clone();

            if (namingRules != null)
            {
                result = logStates.LogState_Success.Clone();

                var searchRegEx = namingRules.Select(nr => new clsObjectAtt
                {
                    ID_Object = nr.ID_Object,
                    ID_AttributeType = attributeTypes.OITem_AttributeType_Regex.GUID
                }).ToList();

                var namingRegex = new List<clsObjectAtt>();

                if (searchRegEx.Any())
                {
                    namingRegex = elSelector.get_Data_ObjectAtt(searchRegEx, boolIDs: false);

                    if (namingRegex == null)
                    {
                        result = logStates.LogState_Error.Clone();
                    }
                }

                if (result.GUID == logStates.LogState_Success.GUID)
                {
                    var testItems = (from obj in objects
                        join namingRule in namingRules on obj.GUID_Parent equals namingRule.ID_Other
                        join regEx in namingRegex on namingRule.ID_Object equals regEx.ID_Object
                        select new {obj, namingRule, regEx}).ToList();

                    var errorItems = testItems.Where(obj =>
                    {
                        var regEx = new Regex(obj.regEx.Val_String);
                        return !regEx.IsMatch(obj.obj.Name);
                    }).ToList();

                    if (!errorItems.Any())
                    {
                        result = elUpdater.save_Objects(objects);
                    }
                    else
                    {
                        if (_namingError != null)
                        {
                            _namingError();    
                        }
                        
                    }
                }
            }

            return result;
        }

        public clsOntologyItem SaeRelationTypes(List<clsOntologyItem> relationTypes)
        {
            foreach (var relationType in relationTypes)
            {
                var result = elUpdater.save_RelationType(relationType);
                if (result.GUID == logStates.LogState_Error.GUID)
                {
                    return logStates.LogState_Error.Clone();
                }

                
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem GetDataRelationTypes(List<clsOntologyItem> relationTypes,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();
                if (doCount)
                {
                    result.Count = elSelector.get_Data_RelationTypesCount(relationTypes);
                }
                else
                {
                    RelationTypes = elSelector.get_Data_RelationTypes(relationTypes);
                    if (RelationTypes == null)
                    {
                        result = logStates.LogState_Error.Clone();
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataAttributeType(List<clsOntologyItem> attributeTypes = null,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_AttributeTypeCount(attributeTypes);
                }
                else
                {
                    AttributeTypes = elSelector.get_Data_AttributeType(attributeTypes);
                    if (AttributeTypes == null)
                    {
                        result = logStates.LogState_Error.Clone();
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataClassAtts(List<clsClassAtt> classAtts,
            bool doIds = false,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ClassAttCount(classAtts);
                }
                else
                {
                    if (doIds)
                    {
                        ClassAttsId = elSelector.get_Data_ClassAtt(classAtts, boolIDs: doIds);
                    }
                    else
                    {
                        ClassAtts = elSelector.get_Data_ClassAtt(classAtts, boolIDs: doIds);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataClassAtts(List<clsOntologyItem> classes = null,
            List<clsOntologyItem> attributeTypes = null,
            bool doIds = false,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ClassAttCount(classes, attributeTypes);
                }
                else
                {
                    if (doIds)
                    {
                        ClassAttsId = elSelector.get_Data_ClassAtt(classes, attributeTypes, boolIDs: doIds);
                    }
                    else
                    {
                        ClassAtts = elSelector.get_Data_ClassAtt(classes, attributeTypes, boolIDs: doIds);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataClassRel(List<clsClassRel> classRels = null,
            bool doIds = false,
            bool doOr = false,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ClassRelCount(classRels);
                }
                else
                {
                    if (doIds)
                    {
                        ClassRelsId = elSelector.get_Data_ClassRel(classRels, true, doOr);
                    }
                    else
                    {
                        ClassRels = elSelector.get_Data_ClassRel(classRels, false, doOr);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                return logStates.LogState_Error.Clone();

            }
            
        }

        public clsOntologyItem GetDataObjectAtt(List<clsObjectAtt> objectAtts = null,
            bool doIds = false,
            bool doCount = false,
            bool doJoin = false,
            bool doJoinClasses = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ObjectAttCount(objectAtts);
                }
                else
                {
                    if (doIds)
                    {
                        ObjAttsId = elSelector.get_Data_ObjectAtt(objectAtts, true, doJoin, doJoinClasses);
                    }
                    else
                    {
                        ObjAtts = elSelector.get_Data_ObjectAtt(objectAtts, false, doJoin, doJoinClasses);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public long GetDataAttOrderId(clsOntologyItem objectItem = null,
            clsOntologyItem attributeType = null,
            bool doAsc = true)
        {
            try
            {
                const string sortField = "OrderID:";

                return elSelector.get_Data_Att_OrderByVal(sortField, objectItem, attributeType, doAsc);
            }
            catch (Exception)
            {

                return 0;
            }
            
        }

        public long GetDataAttOrderByVal(string orderField,
            clsOntologyItem objectItem = null,
            clsOntologyItem attributeType = null,
            bool doAsc = true)
        {
            try
            {
                return elSelector.get_Data_Att_OrderByVal(orderField, objectItem, attributeType, doAsc);
            }
            catch (Exception)
            {

                return 0;
            }
            
        }

        public long GetDataRelOrderId(clsOntologyItem leftItem = null,
            clsOntologyItem rightItem = null,
            clsOntologyItem relationType = null,
            bool doAsc = true)
        {
            return elSelector.get_Data_Rel_OrderByVal(leftItem,
                rightItem,
                relationType,
                "OrderID",
                doAsc);
        }

        public clsOntologyItem GetDataObjectRel(List<clsObjectRel> objectRels,
            bool doIds = false,
            bool doCount = false,
            string direction = null,
            bool doClear = true,
            bool doJoinLeft = false,
            bool doJoinRight = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ObjectRelCount(objectRels);
                }
                else
                {
                    if (doIds)
                    {
                        ObjectRelsId = elSelector.get_Data_ObjectRel(objectRels, true, doJoinLeft, doJoinRight, doClear);
                    }
                    else
                    {
                        ObjectRels = elSelector.get_Data_ObjectRel(objectRels, false, doJoinLeft, doJoinRight, doClear);
                        Objects1 = elSelector.OntologyList_Objects1;
                        Objects2 = elSelector.OntologyList_Objects2;
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataDataTypes(List<clsOntologyItem> dataTypes = null,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_DataTypesCount(dataTypes);
                }
                else
                {
                    DataTypes = elSelector.get_Data_DataTypes(dataTypes);
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataObjectsTree(clsOntologyItem classPar,
            clsOntologyItem classChild,
            clsOntologyItem relationType,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_Objects_Tree_Count(classPar,
                        classChild,
                        relationType);
                }
                else
                {
                    ObjectTree = elSelector.get_Data_Objects_Tree(classPar,
                        classChild,
                        relationType);

                    Objects1 = elSelector.OntologyList_Objects1;
                    Objects2 = elSelector.OntologyList_Objects2;
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetDataObjects(List<clsOntologyItem> objects = null,
            bool doCount = false,
            bool doList2 = false,
            bool clearObj1 = true,
            bool clearObj2 = true,
            bool exact = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (!doList2)
                {
                    Objects1 = elSelector.get_Data_Objects(objects,
                        false,
                        true,
                        true,
                        exact);

                }
                else
                {
                    Objects2 = elSelector.get_Data_Objects(objects,
                        false,
                        true,
                        true,
                        exact);

                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem CreateIndex()
        {
            var indexList = elSelector.IndexList(Server, Port);

            if (!indexList.Any(ix => ix.ToLower() == Index.ToLower()))
            {
                var indexSettings = elSelector.GetIndexSettings();
                var createIndexRequest = new CreateIndexRequest(Index);

                var opResult = elSelector.ElConnector.CreateIndex(createIndexRequest);
                if (opResult.IsValid)
                {
                    return logStates.LogState_Success.Clone();
                }
                else
                {
                    return logStates.LogState_Error.Clone();
                }
            }
            else
            {
                return logStates.LogState_Success.Clone();
            }
            
        }

        public clsOntologyItem CreateReportEs(clsOntologyItem report)
        {
            return logStates.LogState_Success.Clone();
        }

        public bool TestIndexEs()
        {
            var indexExistsRequest = new IndexExistsRequest("Index");

            return elSelector.ElConnector.IndexExists(indexExistsRequest).Exists;
        }

        public clsOntologyItem GetDataClasses(List<clsOntologyItem> classes = null,
            bool doClassesRight = false,
            string sort = null,
            bool doCount = false)
        {
            try
            {
                var result = logStates.LogState_Success.Clone();

                if (doCount)
                {
                    result.Count = elSelector.get_Data_ClassesCount(classes);
                }
                else
                {
                    if (!doClassesRight)
                    {
                        Classes1 = elSelector.get_Data_Classes(classes,
                            false,
                            sort);
                    }
                    else
                    {
                        Classes2 = elSelector.get_Data_Classes(classes,
                            true,
                            sort);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetOItem(string guidItem,
            string typeOfItem)
        {
            try
            {
                var oItem = new clsOntologyItem
                {
                    GUID = guidItem,
                    Type = typeOfItem
                };

                var itemList = new List<clsOntologyItem> { oItem };

                var result = new clsOntologyItem { GUID = logStates.LogState_Error.GUID };

                if (typeOfItem.ToLower() == types.AttributeType.ToLower())
                {
                    result = GetDataAttributeType(itemList);
                    var attributeType = AttributeTypes.FirstOrDefault();
                    if (attributeType != null)
                    {

                        oItem.Name = attributeType.Name;
                        oItem.GUID_Parent = attributeType.GUID_Parent;
                        oItem.GUID_Related = logStates.LogState_Success.GUID;
                        oItem.Type = types.AttributeType;
                    }
                    else
                    {
                        oItem.GUID_Related = logStates.LogState_Error.GUID;
                    }
                }
                else if (typeOfItem.ToLower() == types.ClassType.ToLower())
                {
                    result = GetDataClasses(itemList);

                    var classItem = Classes1.FirstOrDefault();
                    if (classItem != null)
                    {

                        oItem.Name = classItem.Name;
                        oItem.GUID_Parent = classItem.GUID_Parent;
                        oItem.GUID_Related = logStates.LogState_Success.GUID;
                        oItem.Type = types.ClassType;
                    }
                    else
                    {
                        oItem.GUID_Related = logStates.LogState_Error.GUID;
                    }
                }
                else if (typeOfItem.ToLower() == types.ObjectType.ToLower())
                {
                    result = GetDataObjects(itemList);

                    var objectItem = Objects1.FirstOrDefault();
                    if (objectItem != null)
                    {

                        oItem.Name = objectItem.Name;
                        oItem.GUID_Parent = objectItem.GUID_Parent;
                        oItem.GUID_Related = logStates.LogState_Success.GUID;
                        oItem.Type = types.ObjectType;
                    }
                    else
                    {
                        oItem.GUID_Related = logStates.LogState_Error.GUID;
                    }
                }
                else if (typeOfItem.ToLower() == types.RelationType.ToLower())
                {
                    result = GetDataRelationTypes(itemList);

                    var relationItem = RelationTypes.FirstOrDefault();
                    if (relationItem != null)
                    {

                        oItem.Name = relationItem.Name;
                        oItem.GUID_Parent = relationItem.GUID_Parent;
                        oItem.GUID_Related = logStates.LogState_Success.GUID;
                        oItem.Type = types.RelationType;
                    }
                    else
                    {
                        oItem.GUID_Related = logStates.LogState_Error.GUID;
                    }
                }

                if (result.GUID == logStates.LogState_Error.GUID)
                {
                    oItem.GUID_Related = logStates.LogState_Error.GUID;
                }

                return oItem;
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        public string GetClassPath(clsOntologyItem classItem)
        {
            try
            {
                var path = "";

                if (classItem != null)
                {
                    do
                    {
                        if (!string.IsNullOrEmpty(classItem.GUID_Parent))
                        {
                            path = classItem.Name + (!string.IsNullOrEmpty(path) ? @"\" : "") + path;
                            var relClassParent = new List<clsOntologyItem> { new clsOntologyItem { GUID = classItem.GUID_Parent } };

                            var classList = elSelector.get_Data_Classes(relClassParent);

                            if (classList.Any())
                            {
                                classItem = classList.First();
                            }
                            else
                            {
                                classItem = null;
                            }
                        }
                    } while (classItem != null && classItem.GUID_Parent != null);
                }

                if (classItem != null)
                {
                    path = @"\" + classItem.Name + (!string.IsNullOrEmpty(path) ? @"\" : "") + path;
                }

                return path;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

        public clsOntologyItem SaveModel(List<object> itemsToSave)
        {
            var modelWriter = new ModelWriter(this);
            return modelWriter.SaveModel(itemsToSave);
        }

        public OntologyModDBConnector(Globals globals)
        {
            this.server = globals.Server;
            this.port = globals.Port;
            this.index = globals.Index;
            this.indexRep = globals.Index_Rep;
            this.searchRange = globals.SearchRange;
            this.session = globals.Session;

            Initialize();
        }

        public OntologyModDBConnector(string server, int port, string index, string indexRep, int searchRange,
            string session)
        {
            this.server = server;
            this.port = port;
            this.index = index;
            this.indexRep = indexRep;
            this.searchRange = searchRange;
            this.session = session;

            Initialize();
        }

        public OntologyModDBConnector()
        {
            elSelector = new clsDBSelector();
            Initialize();
        }

        private void Initialize()
        {
            Objects1 = new List<clsOntologyItem>();
            Objects2 = new List<clsOntologyItem>();

            ClassAttsId = new List<clsClassAtt>();
            ClassAtts = new List<clsClassAtt>();
            ClassRels = new List<clsClassRel>();
            ClassRelsId = new List<clsClassRel>();
            Classes1 = new List<clsOntologyItem>();
            Classes2 = new List<clsOntologyItem>();
            AttributeTypes = new List<clsOntologyItem>();
            Attributes = new List<clsAttribute>();
            RelationTypes = new List<clsOntologyItem>();
            ObjAtts = new List<clsObjectAtt>();
            ObjAttsId = new List<clsObjectAtt>();
            ObjectRels = new List<clsObjectRel>();
            ObjectRelsId = new List<clsObjectRel>();
            ObjectTree = new List<clsObjectTree>();

            InitializeClient();
        }

        public clsOntologyItem DeleteIndex(string index)
        {
            try
            {
                var deleteIndexRequest = new DeleteIndexRequest(new[] { Index });
                var indexResponse =
                    elSelector.ElConnector.DeleteIndex(deleteIndexRequest);

                return indexResponse.IsValid ? logStates.LogState_Success.Clone() : logStates.LogState_Error.Clone();
            }
            catch (Exception)
            {

                return logStates.LogState_Error.Clone();
            }
            
        }

        private void InitializeClient()
        {
            PackageLength = searchRange;
            elSelector = new clsDBSelector(server,port,index,indexRep,PackageLength,session);
            elDeletor = new clsDBDeletor(elSelector);
            elUpdater = new clsDBUpdater(elSelector);
        }

        public void Dispose()
        {
            
        }
    }
}
