﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector
{
    public class AppConfigItem
    {
        public string ConfigItem { get; set; }
        public string ConfigValue { get; set; }
        public AppConfigItem OriginalItem { get; set; }

        public void CreateOriginal()
        {
            OriginalItem = new AppConfigItem
            {
                ConfigItem = ConfigItem,
                ConfigValue = ConfigValue
            };
        }

        public bool IsModified()
        {
            if (OriginalItem.ConfigItem != ConfigItem ||
                OriginalItem.ConfigValue != ConfigValue)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
