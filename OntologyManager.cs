﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDBConnector
{
    public class OntologyManager : NotifyPropertyChange
    {
        private object managerLocker = new object();

        private clsOntologyItem resultGetOntologies;
        public clsOntologyItem ResultGetOntologies
        {
            get { return resultGetOntologies; }
            set
            {
                resultGetOntologies = value;
                RaisePropertyChanged(nameof(ResultGetOntologies));
            }
        }

        public List<OntologyItem> OntologyItems { get; set; }

        public async Task<clsOntologyItem> GetOntologiesAsync(Globals globals, clsOntologyItem ontology = null)
        {
            var resultOntologies = new ResultOntology();



            lock (managerLocker)
            {
                
                resultOntologies.FilterOntology = ontology;

                resultOntologies = GetOntologies(resultOntologies, globals);

                if (resultOntologies.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultGetOntologies = resultOntologies.Result;
                    return resultOntologies.Result;
                }

                resultOntologies = GetRefsOfOntologies(resultOntologies, globals);

                if (resultOntologies.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultGetOntologies = resultOntologies.Result;
                    return resultOntologies.Result;
                }

                resultOntologies = GetOntologyJoins(resultOntologies, globals);

                if (resultOntologies.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultGetOntologies = resultOntologies.Result;
                    return resultOntologies.Result;
                }
            }

            ResultGetOntologies = resultOntologies.Result;
            return resultOntologies.Result;
        }

        private ResultOntology GetOntologies(ResultOntology resultOntologies, Globals globals)
        {

            var dbReader = new OntologyModDBConnector(globals);

            var searchOntologies = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = resultOntologies.FilterOntology != null ? resultOntologies.FilterOntology.GUID : null,
                        GUID_Parent = globals.Class_Ontologies.GUID
                    }
                };

            resultOntologies.Result = dbReader.GetDataObjects(searchOntologies);

            if (resultOntologies.Result.GUID == globals.LState_Success.GUID)
            {
                resultOntologies.Ontologies = dbReader.Objects1;
            }
            return resultOntologies;
        }

        

        private ResultOntology GetRefsOfOntologies (ResultOntology resultOntologies, Globals globals)
        {
            var dbReader = new OntologyModDBConnector(globals);

            var searchOntologyRefs = resultOntologies.Ontologies.Select(onto => new clsObjectRel
            {
                ID_Object = onto.GUID,
                ID_RelationType = globals.RelationType_belongingResource.GUID
            }).ToList();

            resultOntologies.Result = globals.LState_Success.Clone();
            if (searchOntologyRefs.Any())
            {
                resultOntologies.Result = dbReader.GetDataObjectRel(searchOntologyRefs);
            }
            
            if (resultOntologies.Result.GUID == globals.LState_Success.GUID)
            {
                resultOntologies.RefsOfOntologies = dbReader.ObjectRels;
            }
            return resultOntologies;
        }

        private ResultOntology GetOntologyJoins(ResultOntology resultOntologies, Globals globals)
        {
            var dbReader = new OntologyModDBConnector(globals);

            var searchOntologyJoins = resultOntologies.Ontologies.Select(onto => new clsObjectRel
            {
                ID_Object = onto.GUID,
                ID_RelationType = globals.RelationType_contains.GUID,
                ID_Parent_Other = globals.Class_OntologyJoin.GUID
            }).ToList();

            resultOntologies.Result = globals.LState_Success.Clone();
            if (searchOntologyJoins.Any())
            {
                resultOntologies.Result = dbReader.GetDataObjectRel(searchOntologyJoins);
            }

            if (resultOntologies.Result.GUID == globals.LState_Success.GUID)
            {
                resultOntologies.OJoinsOfOntologies = dbReader.ObjectRels;
            }
            return resultOntologies;
        }

    }

    public class ResultOntology
    {
        public clsOntologyItem Result { get; set; }

        public clsOntologyItem FilterOntology { get; set; }

        public List<clsOntologyItem> Ontologies { get; set; }
        public List<clsObjectRel> RefsOfOntologies { get; set; }
        public List<clsObjectRel> OJoinsOfOntologies { get; set; }

        public List<OntologyItem> OntologyItems
        {
            get
            {
                var result = (from ontology in Ontologies
                              select new OntologyItem
                              {
                                  IdOntology = ontology.GUID,
                                  NameOntology = ontology.Name
                              }).ToList();

                return result;
            }
            
        }

        


        public ResultOntology()
        {
            Ontologies = new List<clsOntologyItem>();
        }
    }

    public class OntologyItem
    {
        public string IdOntology { get; set; }
        public string NameOntology { get; set; }
        public string IdOntologyItem { get; set; }
        public string NameOntologyItem { get; set; }
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        public string IdParent_Ref { get; set; }
        public string TypeRef { get; set; }
        public string IdOntologyRelationRule { get; set; }
        public string NameOntologyRelationRule { get; set; }
        public string IdOItemCreationRule { get; set; }
        public string NameOItemCreationRule { get; set; }
        public long OrderID { get; set; }
    }
}
