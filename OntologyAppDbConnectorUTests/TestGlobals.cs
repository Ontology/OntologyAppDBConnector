﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyAppDbConnectorUTests
{
    public static class TestGlobals
    {
        private static clsLogStates logStates = null;
        public static clsLogStates LogStates
        {
            get
            {
                if (logStates == null)
                {
                    logStates = new clsLogStates();
                }

                return logStates;
            }
        }

        private static clsTypes types;
        public static clsTypes Types
        {
            get
            {
                if (types == null)
                {
                    types = new clsTypes();
                }

                return types;
            }
        }

        private static clsDataTypes dataTypes = null;
        public static clsDataTypes DataTypes
        {
            get
            {
                if (dataTypes == null)
                {
                    dataTypes = new clsDataTypes();
                }

                return dataTypes;
            }
        }

        private static clsClasses classes = null;

        public static clsClasses Classes
        {
            get
            {
                if (classes == null)
                {
                    classes = new clsClasses();
                }

                return classes;
            }
        }

        private static string guidClass = "593B15CAF9594CF2BD633AB2210D06E6";
        private static string guidObject = "FB6DF1D8656B411BADC9C37E1556E9CF";
        private static string guidAttributeTypeBool = "AC4339971A7641DB89285E01AC916B02";
        private static string guidAttributeTypeDateTime = "7054C0D8214F4AA080AFC251EF64CA8A";
        private static string guidAttributeTypeInt = "5B784044F4C8457080A8E349DF97B785";
        private static string guidAttributeTypeReal = "AEFA90BB9E514089AD3347966D9CC97A";
        private static string guidAttributeTypeString = "FE3F5997B957444DBE451EE3D7CC2765";
        private static string guidRelationType = "BC0ABB8D3D004D6AB18451469ABC2D16";
        private static string guidDataType = "3065B9A89A6C44699958A68DA7D2D4D3";
        private static string guidObjectAttBool = "D970B6A44D4D435C84B20C04B252CEA6";
        private static string guidObjectAttDateTime = "9A188CDC1236400D9841BEE44C466F5A";
        private static string guidObjectAttInt = "4C4833CBCB99401DBB1FAC71B40BF7EA";
        private static string guidObjectAttReal = "4B566FD690614FDF9563086539711474";
        private static string guidObjectAttString = "4769B02A33EE49DA9D158F627876CD4D";

        public static string Server => "localhost";

        public static int Port => 9200;

        public static string Index => "ontology_db";
        public static string IndexOutlook => "6f6418ad4f5c4a53baf4f4ae71b48b8174f38566eda64c76b862e839b6a8d07d";
        public static string TypeOutllok => "6f6418ad4f5c4a53baf4f4ae71b48b81";
        public static string IndexSession => "sessiondata";
        public static string TypeSession => "session";

        public static string IndexRep => "reporting"; 

        public static int SearchRange => 5000; 

        public static string Session => Guid.NewGuid().ToString();


        public static clsOntologyItem NewClass()
        {
            return new clsOntologyItem
            {
                GUID = guidClass,
                Name = "TestClass",
                GUID_Parent = Classes.OItem_Class_Root.GUID,
                Type = Types.ClassType
            };
        }

        public static clsClassAtt NewClassAttBool()
        {
            return new clsClassAtt
            {
                ID_AttributeType = NewAttributeTypeBool().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeBool().GUID_Parent,
                Min = 1,
                Max = 1,
                Name_AttributeType = NewAttributeTypeBool().Name,
                Name_Class = NewClass().Name,
                Name_DataType = NewDataType().Name
            };
        }

        public static clsClassAtt NewClassAttDateTime()
        {
            return new clsClassAtt
            {
                ID_AttributeType = NewAttributeTypeDateTime().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeDateTime().GUID_Parent,
                Min = 1,
                Max = 1,
                Name_AttributeType = NewAttributeTypeDateTime().Name,
                Name_Class = NewClass().Name,
                Name_DataType = NewDataType().Name
            };
        }

        public static clsClassAtt NewClassAttInt()
        {
            return new clsClassAtt
            {
                ID_AttributeType = NewAttributeTypeInt().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeInt().GUID_Parent,
                Min = 1,
                Max = 1,
                Name_AttributeType = NewAttributeTypeInt().Name,
                Name_Class = NewClass().Name,
                Name_DataType = NewDataType().Name
            };
        }

        public static clsClassAtt NewClassAttReal()
        {
            return new clsClassAtt
            {
                ID_AttributeType = NewAttributeTypeReal().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeReal().GUID_Parent,
                Min = 1,
                Max = 1,
                Name_AttributeType = NewAttributeTypeReal().Name,
                Name_Class = NewClass().Name,
                Name_DataType = NewDataType().Name
            };
        }

        public static clsClassAtt NewClassAttString()
        {
            return new clsClassAtt
            {
                ID_AttributeType = NewAttributeTypeString().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeString().GUID_Parent,
                Min = 1,
                Max = 1,
                Name_AttributeType = NewAttributeTypeString().Name,
                Name_Class = NewClass().Name,
                Name_DataType = NewDataType().Name
            };
        }

        public static clsClassRel NewClassRel()
        {
            return new clsClassRel
            {
                ID_Class_Left = NewClass().GUID,
                ID_Class_Right = NewClass().GUID,
                ID_RelationType = NewRelationType().GUID,
                Ontology = Types.ClassType,
                Min_Forw = 1,
                Max_Backw = 1,
                Max_Forw = 1
            };
        }

        public static clsObjectAtt NewObjectAttBool()
        {
            return new clsObjectAtt
            {
                ID_Attribute = guidObjectAttBool,
                ID_AttributeType = NewAttributeTypeBool().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeBool().GUID_Parent,
                ID_Object = NewObject().GUID,
                OrderID = 1,
                Val_Bit = true,
                Val_Named = true.ToString()
            };
        }

        public static clsObjectAtt NewObjectAttDateTime()
        {
            return new clsObjectAtt
            {
                ID_Attribute = guidObjectAttDateTime,
                ID_AttributeType = NewAttributeTypeDateTime().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeDateTime().GUID_Parent,
                ID_Object = NewObject().GUID,
                OrderID = 1,
                Val_Datetime = DateTime.Now,
                Val_Named = DateTime.Now.ToString()
            };
        }

        public static clsObjectAtt NewObjectAttInt()
        {
            return new clsObjectAtt
            {
                ID_Attribute = guidObjectAttInt,
                ID_AttributeType = NewAttributeTypeInt().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeInt().GUID_Parent,
                ID_Object = NewObject().GUID,
                OrderID = 1,
                Val_Int = 14098,
                Val_Named = 14098.ToString()
            };
        }

        public static clsObjectAtt NewObjectAttReal()
        {
            return new clsObjectAtt
            {
                ID_Attribute = guidObjectAttReal,
                ID_AttributeType = NewAttributeTypeReal().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeReal().GUID_Parent,
                ID_Object = NewObject().GUID,
                OrderID = 1,
                Val_Real = 14098.1,
                Val_Named = (14098.1).ToString()
            };
        }

        public static clsObjectAtt NewObjectAttString()
        {
            return new clsObjectAtt
            {
                ID_Attribute = guidObjectAttString,
                ID_AttributeType = NewAttributeTypeString().GUID,
                ID_Class = NewClass().GUID,
                ID_DataType = NewAttributeTypeString().GUID_Parent,
                ID_Object = NewObject().GUID,
                OrderID = 1,
                Val_String = "14098",
                Val_Named = "14098"
            };
        }

        public static clsObjectRel NewObjectRel()
        {
            return new clsObjectRel
            {
                ID_Object = NewObject().GUID,
                ID_Other = NewObject().GUID,
                ID_Parent_Object = NewObject().GUID_Parent,
                ID_Parent_Other = NewObject().GUID_Parent,
                ID_RelationType = NewRelationType().GUID,
                Ontology = Types.ObjectType,
                OrderID = 1
            };
        }

        public static clsOntologyItem NewObject()
        {
            return new clsOntologyItem
            {
                GUID = guidObject,
                Name = "TestObject",
                GUID_Parent = NewClass().GUID,
                Type = Types.ObjectType
            };
        }

        public static clsOntologyItem NewAttributeTypeBool()
        {
            return new clsOntologyItem
            {
                GUID = guidAttributeTypeBool,
                Name = "TestAttributeTypeBool",
                GUID_Parent = DataTypes.DType_Bool.GUID,
                Type = Types.AttributeType
            };
        }

        public static clsOntologyItem NewAttributeTypeDateTime()
        {
            return new clsOntologyItem
            {
                GUID = guidAttributeTypeDateTime,
                Name = "TestAttributeTypeDatetime",
                GUID_Parent = DataTypes.DType_DateTime.GUID,
                Type = Types.AttributeType
            };
        }

        public static clsOntologyItem NewAttributeTypeInt()
        {
            return new clsOntologyItem
            {
                GUID = guidAttributeTypeInt,
                Name = "TestAttributeTypeInt",
                GUID_Parent = DataTypes.DType_Int.GUID,
                Type = Types.AttributeType
            };
        }

        public static clsOntologyItem NewAttributeTypeReal()
        {
            return new clsOntologyItem
            {
                GUID = guidAttributeTypeReal,
                Name = "TestAttributeTypeReal",
                GUID_Parent = DataTypes.DType_Real.GUID,
                Type = Types.AttributeType
            };
        }

        public static clsOntologyItem NewAttributeTypeString()
        {
            return new clsOntologyItem
            {
                GUID = guidAttributeTypeString,
                Name = "TestAttributeTypeString",
                GUID_Parent = DataTypes.DType_String.GUID,
                Type = Types.AttributeType
            };
        }
        public static clsOntologyItem NewDataType()
        {
            return new clsOntologyItem
            {
                GUID = guidDataType,
                Name = "TestDataType",
                Type = Types.ObjectType
            };
        }

        public static clsOntologyItem NewRelationType()
        {
            return new clsOntologyItem
            {
                GUID = guidRelationType,
                Name = "TestRelationType",
                Type = Types.ObjectType
            };
        }


        private static string guidAppDoc = "973D8CDF1F6C4E5B81089DF6E9FAE692";


        public static clsAppDocuments NewAppDoc()
        {
            var dict = new Dictionary<string, object>();

            dict.Add("EntryId", "abcdefg");
            dict.Add("CreationDate", DateTime.Now);
            dict.Add("Folder", "Posteingang");
            dict.Add("SemItemPresent", false);
            dict.Add("SenderEmailAddress", "tassilo.koller@explido.de");
            dict.Add("SenderName", "Tassilo Koller");
            dict.Add("Subject", "Termin Dr. Brenner: Do. 18:00");
            dict.Add("To", "tasilok@gmx.de");

            return new clsAppDocuments
            {
                Id = guidAppDoc,
                Dict = dict
            };
        }

        public static int CountAttributeTypes => 345;
        public static int CountRelationTypes => 643;
        public static int CountClasses => 2031;

        public static int CountObjects => 503395;

        public static int CountClassAtts => 532;

        public static int CountClassRels => 2928;

        public static int CountObjectAtts => 545284;

        public static int CountObjectRels => 984342;
    }
}
