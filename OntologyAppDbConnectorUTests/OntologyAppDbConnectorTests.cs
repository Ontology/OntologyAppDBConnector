﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntologyAppDBConnector;

namespace OntologyAppDbConnectorUTests
{
    [TestClass]
    public class OntologyAppDbConnectorTests
    {
        [TestMethod]
        public void TestCreateIndex()
        {
            var oMod = new OntologyModDBConnector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);
            oMod.CreateIndex();

        }

        [TestMethod]
        public void TestGetConfigValue()
        {
            var globals = new Globals();
            var resultItem = globals.GetConfigValue("OModulesUrl");

            if (resultItem.GUID == globals.LState_Error.GUID)
            {
                Assert.Fail(resultItem.Additional1);
            }
        }
    }
}
